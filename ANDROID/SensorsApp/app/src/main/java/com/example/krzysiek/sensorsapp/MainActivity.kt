package com.example.krzysiek.sensorsapp

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import java.util.*
import android.os.Vibrator



class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var sensorManager:SensorManager
    private var lastUpdate: Long =0
    val random = Random()
    var currentImage:Int=R.drawable.kostka
    var dark:Boolean=false
    private val LIGHTDARKLIMIT=50

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sensorManager=  getSystemService(Context.SENSOR_SERVICE) as SensorManager

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL )
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL )

        lastUpdate = System.currentTimeMillis();
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        val sensor:Sensor=p0!!.sensor

            if (sensor.type == Sensor.TYPE_LIGHT) {
                if (p0.values[0]>LIGHTDARKLIMIT){
                    if(dark==true){
                        changeImagesColor()
                        dark = false
                        changeBackgroundColor()
                    }
                }else {
                    if (dark == false){
                        changeImagesColor()
                        dark = true
                        changeBackgroundColor()
                    }
                }
            }else
                if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    getAccelerometer(p0);
                }
    }
    fun rand() : Int {
        return random.nextInt(7 - 1) + 1
    }
    fun changeBackgroundColor(){
        if (dark==true) {
            this.getWindow().getDecorView().setBackgroundColor(resources.getColor(R.color.black))
        }else
            this.getWindow().getDecorView().setBackgroundColor(resources.getColor(R.color.white))
    }
    fun setImage(){
        if (dark==true)
        {
            when (rand()){
                1-> currentImage=R.drawable.done
                2-> currentImage=R.drawable.dtwo
                3-> currentImage=R.drawable.dthree
                4-> currentImage=R.drawable.dfour
                5-> currentImage=R.drawable.dfive
                6-> currentImage=R.drawable.dsix
                else -> currentImage=R.drawable.six
            }
        }else
        {
            when (rand()){
                1-> currentImage=R.drawable.one
                2-> currentImage=R.drawable.two
                3-> currentImage=R.drawable.three
                4-> currentImage=R.drawable.four
                5-> currentImage=R.drawable.five
                6-> currentImage=R.drawable.six
                else -> currentImage=R.drawable.six
            }
        }
        findViewById<ImageView>(R.id.square).setImageResource(currentImage)
    }

    fun changeImagesColor(){
       when(currentImage){
           R.drawable.one-> currentImage=R.drawable.done
           R.drawable.two-> currentImage=R.drawable.dtwo
           R.drawable.three-> currentImage=R.drawable.dthree
           R.drawable.four-> currentImage=R.drawable.dfour
           R.drawable.five-> currentImage=R.drawable.dfive
           R.drawable.six-> currentImage=R.drawable.dsix
           R.drawable.kostka-> currentImage=R.drawable.dkostka
           R.drawable.done-> currentImage=R.drawable.one
           R.drawable.dtwo-> currentImage=R.drawable.two
           R.drawable.dthree-> currentImage=R.drawable.three
           R.drawable.dfour-> currentImage=R.drawable.four
           R.drawable.dfive-> currentImage=R.drawable.five
           R.drawable.dsix-> currentImage=R.drawable.six
           R.drawable.dkostka-> currentImage=R.drawable.kostka
       }
        findViewById<ImageView>(R.id.square).setImageResource(currentImage)
    }
    private fun getAccelerometer(event: SensorEvent) {
        val values = event.values

        val x = values[0]
        val y = values[1]
        val z = values[2]

        val accelationSquareRoot = (x * x + y * y + z * z) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH)
        val actualTime = System.currentTimeMillis()// event.timestamp
        if (accelationSquareRoot >= 2)
        {
            if (actualTime - lastUpdate > 500) {
                lastUpdate = actualTime
                setImage()
                val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(500)
            }
        }
    }
}
