package com.example.krzysiek.musicplayer

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.ContentUris
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.os.PowerManager
import android.util.Log




/**
 * Created by Krzysiek on 2018-05-20.
 */
class MusicService : Service(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
MediaPlayer.OnCompletionListener {

    companion object {
        private lateinit var player: MediaPlayer
        private lateinit var songs: ArrayList<Song>
        private var songPosition: Int = 0
        private lateinit var musicBinder: IBinder
        private var songTitle: String = ""
        private val NOTIFY_ID = 1
    }

    override fun onCreate() {
        super.onCreate()
        musicBinder = MusicBinder()
        player = MediaPlayer()
        initMusicPlayer()
    }


    override fun onBind(intent: Intent): IBinder? {
        return musicBinder
    }

    fun setList(theSongs: ArrayList<Song>) {
        songs = theSongs
    }

    inner class MusicBinder : Binder() {
        internal val service: MusicService
            get() = this@MusicService

    }


    override fun onUnbind(intent: Intent?): Boolean {
        player.stop()
        //player.release()
        return false
    }

    fun playSong(){
        player.reset()
        var playSong:Song = songs.get(songPosition)
        songTitle=playSong.title
        var currSong : Long = playSong.getId()
        var trackUri : Uri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong)
        try {
            player.setDataSource(applicationContext, trackUri)
        }
        catch (e:Exception ){
            Log.e("MUSIC SERVICE", "Error setting data source",e)
        }
        player.prepareAsync()
    }

    fun initMusicPlayer(){
        player.setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        player.setOnPreparedListener(this@MusicService)
        player.setOnCompletionListener(this@MusicService)
        player.setOnErrorListener(this@MusicService)
    }

    override fun onCompletion(mp: MediaPlayer?) {
        if(player.currentPosition==player.duration){
            mp?.reset()
            playNext()
        }
    }


    override fun onPrepared(mp: MediaPlayer?) {
        mp!!.start()
    }

    fun getPosn(): Int {
        return player.currentPosition
    }


    fun go() {
        player.start()
    }
    fun playPrev(){
        songPosition--
        if (songPosition<0)
            songPosition=songs.size-1

        playSong()
    }
    fun playNext(){
        ++songPosition
        if(songPosition>=songs.size)
            songPosition=0
        playSong()
    }

    //-----------------------------------------
    fun getDur(): Int {
        return player.duration
    }
    fun isPng(): Boolean {
        return player.isPlaying
    }
    fun pausePlayer() {
        player.pause()
    }
    fun setSong(songIndex: Int) {
        songPosition = songIndex
    }
    fun seek(posn: Int) {
        player.seekTo(posn)
    }
    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        mp!!.reset()
        return false
    }

}