package com.example.krzysiek.musicplayer;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.support.v4.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {
    private static final String PREFERENCES_NAME = "myPreferences";
    private static final String SORTING = "sorting";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        SwitchPreference sorting = (SwitchPreference) findPreference("sortowanie");
        if (sorting != null) {
            sorting.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference arg0, Object isVibrateOnObject) {
                    try {
                        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences(PREFERENCES_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        boolean isVibrateOn = (Boolean) isVibrateOnObject;
                        editor.putBoolean(SORTING, isVibrateOn);
                        editor.apply();
                    }
                    catch (Exception e) {
                    }
                    return true;
                }
            });
        }
    }

}
