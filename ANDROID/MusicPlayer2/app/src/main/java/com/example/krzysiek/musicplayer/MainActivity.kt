package com.example.krzysiek.musicplayer

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.song_item.view.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.content.ComponentName
import com.example.krzysiek.musicplayer.MusicService.MusicBinder
import android.os.IBinder
import android.content.ServiceConnection
import android.preference.PreferenceManager
import android.view.*
import android.widget.MediaController
import java.util.concurrent.TimeUnit




class MainActivity : AppCompatActivity() , MediaController.MediaPlayerControl {

    companion object {
        private val PREFERENCES_NAME = "myPreferences"
        private val SORTING = "sorting"
        private lateinit var recyclerView: RecyclerView
        private lateinit var viewAdapter: RecyclerView.Adapter<*>
        private lateinit var viewManager: RecyclerView.LayoutManager
        private var musicService: MusicService? = null
        private var playIntent: Intent? = null
        private var musicBound: Boolean = false
        private val songPlayList: ArrayList<Song> = ArrayList()
        private val PERMISSION_REQUEST_CODE = 12
        private var controller: MusicController? = null
        private var paused = false
        private var playbackPaused = false
    }

    fun selector(p: Song): String = p.title
    fun selector2(p: Song): Long = p.duration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeComponents()
        setController()

    }

    override fun onResume() {
        super.onResume()

        if (paused) {
            setController()
            paused = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        this.invalidateOptionsMenu()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.aboutMe) {
            val intentAboutMe = Intent(this, AboutMeActivity::class.java)
            startActivity(intentAboutMe)
            return true
        }
        if (id == R.id.settings) {
            val intentSettings = Intent(this, SettingsActivity::class.java)
            startActivity(intentSettings)
            return true
        }

        return super.onOptionsItemSelected(item)
    }
    override fun onStart() {
        super.onStart()
        if (playIntent == null) {
            var musicConnection: ServiceConnection = object : ServiceConnection {
                override fun onServiceDisconnected(name: ComponentName?): Unit {
                    musicBound = false
                }

                override fun onServiceConnected(name: ComponentName?, service: IBinder?): Unit {
                    val binder = service as MusicBinder
                    musicService = binder.service
                    musicService?.setList(songPlayList)
                    musicBound = true
                }
            }

            playIntent = Intent(getApplicationContext(), MusicService::class.java)
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE)
            startService(playIntent)
        }
        val settings = getApplicationContext().getSharedPreferences(PREFERENCES_NAME, 0)
        var isSorting= settings.getBoolean(SORTING, false)
        if (isSorting) {
            songPlayList.sortBy({selector(it)})
        }else
            songPlayList.sortBy({selector2(it)})

    }



    override fun onPause() {
        super.onPause()
        paused=true;
    }

    override fun onStop() {
        controller?.hide()
        super.onStop()
    }

    override fun pause() {
        playbackPaused=true
        musicService?.pausePlayer()
        setController()
    }

    override fun start() {
        musicService!!.go()
        playbackPaused = false
        setController()
    }

    private fun playNext() {
        musicService?.playNext()
        if(playbackPaused){
            setController()
            playbackPaused=false
        }
    }

    private fun playPrev() {
        musicService!!.playPrev()
        if(playbackPaused){
            setController()
            playbackPaused=false
        }
    }

    fun songPicked(pos: Int) {
        musicService?.setSong(pos)

        playbackPaused=false


        musicService?.playSong()
        controller?.show(0)
        setController()
    }

    //-------------------------------------------------------------------------
    override fun isPlaying(): Boolean {
        if(musicService!=null && musicBound) {
            return musicService!!.isPng()
        }
        else{
            return false
        }
    }
    private fun initializeComponents(){
        if(ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
        }else {
            getSongList()
        }
        viewManager = LinearLayoutManager(this)
        viewAdapter = SongPlayersAdapter(songPlayList,this)
        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewSong).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }
    private fun getSongList() {
        songPlayList.clear()
        var songCursor: Cursor? = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null)
        var titleColumn = songCursor?.getColumnIndex(MediaStore.Audio.Media.TITLE)
        var idColumn = songCursor?.getColumnIndex(MediaStore.Audio.Media._ID)
        var authorColumn = songCursor?.getColumnIndex(MediaStore.Audio.Media.ARTIST)
        var durationColumn = songCursor?.getColumnIndex(MediaStore.Audio.Media.DURATION)

        while (songCursor != null && songCursor.moveToNext()) {
            var songName = songCursor.getString(titleColumn!!)
            var duration = songCursor.getLong(durationColumn!!)
            var author = songCursor.getString(authorColumn!!)
            var id = songCursor.getLong(idColumn!!)
            songPlayList.add(Song(id, songName, author, duration))
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getSongList()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
    private fun setController() {
        controller = MusicController(this)
        controller?.setMediaPlayer(this)
        controller?.setAnchorView(findViewById<RecyclerView>(R.id.recyclerViewSong))
        controller?.setEnabled(true)
        controller!!.setPrevNextListeners(View.OnClickListener { playNext() }, View.OnClickListener { playPrev() })
    }
    override fun canSeekForward(): Boolean {
        return true
    }
    override fun getAudioSessionId(): Int {
        return 0
    }
    override fun canPause(): Boolean {
        return true
    }
    override fun canSeekBackward(): Boolean {
        return true
    }
    override fun getCurrentPosition(): Int {
        if(musicService!=null && musicBound && (musicService!!.isPng() || playbackPaused)) {
            return musicService!!.getPosn()
        }
        else {
            return 0
        }
    }
    override fun seekTo(pos: Int) {
        musicService!!.seek(pos)
    }
    override fun getBufferPercentage(): Int {
        return 0
    }
    override fun getDuration(): Int {
        if(musicService!=null && musicBound && (musicService!!.isPng() || playbackPaused)){
            return musicService!!.getDur()
        }
        else{
            return 0
        }
    }


    inner class SongPlayersAdapter(val items : ArrayList<Song>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            holder?.title?.text = items.get(position).title
            holder?.author?.text = items.get(position).author
            holder?.duration?.text =  convertDuration(items.get(position).duration)
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.song_item, parent, false)
            val viewHolder = ViewHolder(view)

            view.setOnClickListener{
                songPicked(viewHolder.adapterPosition)
            }
            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        /*override fun getItemId(position: Int): Long {
            return super.getItemId(position)
        }*/
        private fun convertDuration(milis:Long):String{
            var duration = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(milis),
                    TimeUnit.MILLISECONDS.toSeconds(milis)-TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis))
            )
            return duration
        }
    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.title
        val author = itemView.author
        val duration = itemView.duration
    }
}
