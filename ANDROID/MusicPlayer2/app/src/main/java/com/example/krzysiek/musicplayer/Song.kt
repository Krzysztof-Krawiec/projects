package com.example.krzysiek.musicplayer

/**
 * Created by Krzysiek on 2018-05-19.
 */
class Song(private var id:Long, var title: String, var author: String, var duration:Long){

    fun getId():Long{
        return id
    }
}