package com.example.stud.bmi;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void BMI_isCorrect() throws Exception {
        MetricDimensionsBMI mdBMI = new MetricDimensionsBMI(80,160);
        assertEquals(  31.25, mdBMI.count(),0.001);
    }
    @Test
    public void BMI_isCorrect2() throws Exception {
        MetricDimensionsBMI mdBMI = new MetricDimensionsBMI(60,150);
        assertEquals(  26.66, mdBMI.count(),0.01);
    }
    @Test
    public void BMI_isCorrect3() throws Exception {
        EnglishDimensionsBMI edBMI = new EnglishDimensionsBMI(220,60);
        assertEquals(  42.96, edBMI.count(),0.01);
    }

    @Test (expected = IllegalArgumentException.class)
    public void BMI_isCorrect4() throws Exception {
        EnglishDimensionsBMI edBMI = new EnglishDimensionsBMI(220,10);
        assertEquals(  42.96, edBMI.count(),0.01);
    }

    @Test (expected = IllegalArgumentException.class)
    public void BMI_Excpetion() throws Exception
    {
        MetricDimensionsBMI mdBMI = new MetricDimensionsBMI(60,0);
        assertEquals(  26.66, mdBMI.count(),0.01);
    }
}