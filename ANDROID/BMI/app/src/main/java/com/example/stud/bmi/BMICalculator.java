package com.example.stud.bmi;

import java.text.DecimalFormat;

/**
 * Created by Krzysiek on 2018-03-07.
 */
//sharedpreferences do zapisu damych wpisanych
abstract class BMICalculator {
    protected double height;
    protected double weight;

    BMICalculator(double dWeight, double dHeight)
    {
        weight = dWeight;
        height = dHeight;
    }
    abstract void setHeight(double dHeight);
    abstract void setWeight(double dWeight);
    abstract double setHeight();
    abstract double setWeight();
    abstract double count();
    abstract boolean validateData();


}
