package com.example.krzysiek.szkola



import android.os.Bundle
import android.support.v4.app.*
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView


class DetailsActivity : AppCompatActivity() {

    companion object {
        private val NUMBER_OF_FRAGMENT_PAGES= 2
    }
    private var imagesList=ArrayList<Int>()
    private var studentsList=ArrayList<Student>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        receiveData();


    }
    private fun receiveData(){
        val myIntent = this.intent
        val receivedElement = myIntent.getParcelableExtra<SchoolClass>(R.string.schoolClassObject.toString())

        imagesList=receivedElement.classImages
        studentsList= receivedElement.students
        val pager = findViewById<ViewPager>(R.id.pager)
        pager.adapter = MyPagerAdapter(supportFragmentManager)

        findViewById<TextView>(R.id.className).setText(receivedElement.className)
        findViewById<TextView>(R.id.tutorName).setText(receivedElement.tutor)
        findViewById<ImageView>(R.id.classImage).setImageResource(receivedElement.image)
    }

    private inner class MyPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

        override fun getItem(pos: Int): Fragment {
            return if (pos == 0)
                GalleryFragment.newInstance(imagesList)
            else
                StudentsFragment.newInstance(studentsList)
        }

        override fun getCount(): Int {
            return NUMBER_OF_FRAGMENT_PAGES
        }
    }
}

