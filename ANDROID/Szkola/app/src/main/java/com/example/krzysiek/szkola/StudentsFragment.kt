package com.example.krzysiek.szkola

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.student.view.*
import kotlinx.android.synthetic.main.students_fragment.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [StudentsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [StudentsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StudentsFragment : Fragment() {

    companion object {
        private var studentsList = ArrayList<Student>()
        private var studentsAdapter: StudentsAdapter? = null

        fun newInstance(studentsListReceived: ArrayList<Student>): StudentsFragment
        {
            studentsList = studentsListReceived
            return StudentsFragment()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.students_fragment,container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        studentsAdapter = StudentsAdapter(activity, studentsList)
        ListViewStudents.adapter = studentsAdapter
    }


    inner class StudentsAdapter : BaseAdapter {
        private var studentsList = ArrayList<Student>()
        private var context: Context? = null

        constructor(context: Context?, studentsList: ArrayList<Student>) : super() {
            this.studentsList = studentsList
            this.context = context
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            val newConvertView = convertView ?: layoutInflater.inflate(R.layout.student, parent, false)

            val element=getItem(position)
            newConvertView.name?.text = element.name
            newConvertView.vorname?.text = element.vorname

            return newConvertView
        }
        override fun getItem(position: Int): Student {
            return studentsList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return studentsList.size
        }

    }
}
