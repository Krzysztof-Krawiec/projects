package com.example.krzysiek.szkola

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Krzysiek on 2018-03-28.
 */
class SchoolClass (val className: String, val tutor:String, val image:Int, val students:ArrayList<Student>,val classImages:ArrayList<Int>): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            arrayListOf<Student>().apply {
                parcel.readList(this, Student::class.java.classLoader)
            },
            arrayListOf<Int>().apply {
                parcel.readList(this, Int::class.java.classLoader)
            })

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(className)
        parcel.writeString(tutor)
        parcel.writeInt(image)
        parcel.writeList(students)
        parcel.writeList(classImages)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolClass> {
        override fun createFromParcel(parcel: Parcel): SchoolClass {
            return SchoolClass(parcel)
        }

        override fun newArray(size: Int): Array<SchoolClass?> {
            return arrayOfNulls(size)
        }
    }
}