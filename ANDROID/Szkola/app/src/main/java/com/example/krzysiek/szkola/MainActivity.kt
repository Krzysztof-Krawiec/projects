package com.example.krzysiek.szkola

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.class_list_item.view.*
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private val schoolClasses: ArrayList<SchoolClass> = ArrayList()
    companion object {
        private lateinit var recyclerView: RecyclerView
        private lateinit var viewAdapter: RecyclerView.Adapter<*>
        private lateinit var viewManager: RecyclerView.LayoutManager
        val NUMBER_OF_IMAGES = 6



    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeComponents()
        initializeSwiping()
    }
    private fun initializeComponents(){
        fillSchoolClasses()
        viewManager = LinearLayoutManager(this)
        viewAdapter = SchoolClassesAdapter(schoolClasses,this)
        recyclerView = findViewById<RecyclerView>(R.id.ClassesView).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }


    }
    private fun initializeSwiping(){
        val onSwipeCallback = object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT ){
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                onElementSwiped(viewHolder)
            }
        }
        val itemSwipeHelper = ItemTouchHelper(onSwipeCallback)
        itemSwipeHelper.attachToRecyclerView(recyclerView)
    }
    private fun onElementSwiped(viewHolder: RecyclerView.ViewHolder?){
        viewHolder?.let{
            schoolClasses.removeAt(viewHolder.adapterPosition)
            viewAdapter.notifyDataSetChanged()
        }
    }
    private fun fillSchoolClasses(){
        val images = arrayOf<Int>(
                R.drawable.kolor1,
                R.drawable.kolor2,
                R.drawable.kolor3,
                R.drawable.kolor4,
                R.drawable.kolor5,
                R.drawable.kolor6,
                R.drawable.kolor7,
                R.drawable.kolor8,
                R.drawable.kolor9)
        fun Random.nextInt(range: IntRange): Int {
            return range.start + nextInt(range.last - range.start)
        }
        fun rand(range: IntRange): Int {
            return ThreadLocalRandom.current().nextInt(range)
        }
        fun getNumber():Int{
            return images[rand(0..(images.size-1))]
        }
        fun getImagesList():ArrayList<Int>{
            val imageList = ArrayList<Int>()
            for (i in 0..(NUMBER_OF_IMAGES -1))
            {
                imageList.add(getNumber())
            }
            return imageList
        }
        val students=ArrayList<Student>()
        students.add(Student("Adam","Kornacki"))
        students.add(Student("Małgorzata","Abacka"))
        students.add(Student("Wojtek","Wojtkiewicz"))
        students.add(Student("Halina","Kisielewicz"))
        students.add(Student("Patryk","Wojciechowski"))
        schoolClasses.add(SchoolClass("Ia","Tomasz Dębowski", getNumber(), students,ArrayList<Int>(getImagesList())))
        schoolClasses.add(SchoolClass("IIa","Anna Tomaszewska", getNumber(), students,ArrayList<Int>(getImagesList())))
        schoolClasses.add(SchoolClass("IIIa","Irena Zawadzka", getNumber(), students,ArrayList<Int>(getImagesList())))
        schoolClasses.add(SchoolClass("IVa","Rafał Ochocki", getNumber(), students,ArrayList<Int>(getImagesList())))
        schoolClasses.add(SchoolClass("Va","Patrycja Garbowicz", getNumber(), students,ArrayList<Int>(getImagesList())))


    }

    class SchoolClassesAdapter(val items : ArrayList<SchoolClass>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            holder?.className?.text = items.get(position).className
            holder?.tutor?.text = items.get(position).tutor
            holder?.classImage?.setImageResource(items.get(position).image)
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.class_list_item, parent, false)
            val viewHolder = ViewHolder(view)

            view.setOnClickListener{
                val intent = Intent(view.context, DetailsActivity::class.java)
                intent.putExtra(R.string.schoolClassObject.toString(), items[viewHolder.adapterPosition])
                view.context.startActivity(intent)
            }

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun getItemId(position: Int): Long {
            return super.getItemId(position)
        }

    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view)  {
        val className = view.className
        val tutor = view.tutor
        val classImage = view.classImage
    }
}
