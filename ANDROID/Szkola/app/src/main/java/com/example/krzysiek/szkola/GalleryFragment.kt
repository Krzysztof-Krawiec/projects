package com.example.krzysiek.szkola


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView


class GalleryFragment : Fragment() {


    companion object {
        var images=ArrayList<Int>()

        fun newInstance(listClassImages:ArrayList<Int>): GalleryFragment {
            images=listClassImages
            return GalleryFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.gallery_fragment, container,false)
    }
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gridview: GridView =  view!!.findViewById(R.id.galleryGrid)
        gridview.adapter = ImageAdapter(context)
    }

    class ImageAdapter(private val mContext: Context) : BaseAdapter() {

        override fun getCount(): Int = images.size

        override fun getItem(position: Int): Any? = null

        override fun getItemId(position: Int): Long = 0L

        // create a new ImageView for each item referenced by the Adapter
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val imageView: ImageView
            if (convertView == null) {
                imageView = ImageView(mContext)
            } else {
                imageView = convertView as ImageView
            }
            imageView.setImageResource(images[position])
            return imageView
        }
    }
}