package com.example.krzysiek.zadanie2

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import android.widget.EditText
import java.time.Year
import java.util.*

class AddItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_item_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.saveItem -> {
                var name:String= (findViewById<EditText>(R.id.etName) as EditText).text.toString()
                var vorname:String = (findViewById<EditText>(R.id.etVorname) as EditText).text.toString()
                val person:People=People(name,vorname)//,date)
                val returnIntent : Intent= Intent()
                returnIntent.putExtra("person", person)
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}
