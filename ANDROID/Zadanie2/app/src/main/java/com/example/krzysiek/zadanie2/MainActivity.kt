package com.example.krzysiek.zadanie2

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.view.Menu
import android.view.MenuItem


class MainActivity : AppCompatActivity() {
    private var peopleList = ArrayList<Person>()
    private var peopleAdapter : PeopleAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        peopleList.add(Person(1, "Krawiec1", "Krzysztof", 1997))
        peopleList.add(Person(2, "Krawiec2", "Krzysztof", 1997))
        peopleList.add(Person(3, "Krawiec3", "Krzysztof", 1997))
        peopleList.add(Person(4, "Krawiec4", "Krzysztof", 1997))
        peopleAdapter = PeopleAdapter(this, peopleList)
        ListViewPeople.adapter = peopleAdapter
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.addItem -> {
                val intent:Intent =Intent(this,AddItemActivity::class.java)
                //startActivity(Intent(this, AddItemActivity::class.java))
                startActivityForResult(Intent(this, AddItemActivity::class.java),1)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    inner class PeopleAdapter : BaseAdapter {

        private var peopleList = ArrayList<Person>()
        private var context: Context? = null

        constructor(context: Context, peopleList: ArrayList<Person>) : super() {
            this.peopleList = peopleList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.person, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }
            vh.tvName.text = peopleList[position].name + " "+peopleList[position].vorname
            vh.tvBirthYear.text = peopleList[position].birthYear.toString()

            view?.findViewById<Button>(R.id.removeButton)?.setOnClickListener {
                peopleList.removeAt(position)
                notifyDataSetChanged()
            }
            return view
        }

        override fun getItem(position: Int): Any {
            return peopleList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return peopleList.size
        }

    }

    private class ViewHolder(view: View?) {
        val tvName: TextView
        val tvBirthYear: TextView

        init {
            this.tvName = view?.findViewById<TextView>(R.id.name) as TextView
            this.tvBirthYear = view.findViewById<TextView>(R.id.birthYear) as TextView
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)
        if (data !=null){
            if (resultCode == RESULT_OK) {
                val returnedPerson: People = data.getSerializableExtra("person") as People

                peopleList.add(Person(0, returnedPerson.name,returnedPerson.vorname,0))
                peopleAdapter?.notifyDataSetChanged()
            }
        }

    }
}


