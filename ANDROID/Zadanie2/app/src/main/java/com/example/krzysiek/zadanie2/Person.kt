package com.example.krzysiek.zadanie2

import android.widget.EditText
import java.io.Serializable
import java.util.*

/**
 * Created by Krzysiek on 2018-03-18.
 */
class Person{
    var id: Int? = null
    var name: String? =null
    var vorname: String?=null
    var birthYear: Int?=null

    constructor(id: Int, name: String, vorname: String, birthYear: Int) {
        this.id = id
        this.name = name
        this.vorname = vorname
        this.birthYear = birthYear
    }
}

data class People(
    val name: String,
    val vorname: String
    //val birthYear: Date =Date()
): Serializable{}