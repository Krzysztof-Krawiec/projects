/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.krzysiek.androidtvgallery

object ImageList {
    val MOVIE_CATEGORY = arrayOf(
            "Mercedes-Benz",
            "Porsche")

    val list: List<Image> by lazy {
        setupImages()
    }
    private var count: Long = 0

    private fun setupImages(): List<Image> {

//        val mercedes = arrayOf(
//                "https://www.mercedes-benz.com/wp-content/uploads/sites/3/2018/05/00-mercedes-benz-vehicles-mbsocialcar-2018-mercedes-amg-s-65-coupe-c-217-facelift-obsidian-black-metallic-2560x1440-848x477.jpg",
//                "https://www.mercedes-benz.com/wp-content/uploads/sites/3/2018/05/mercedes-amg-gt-s-roadster-2018-r-190-3400x1440.jpg",
//                "https://m.mercedes-benz.pl/content/media_library/hq/hq_mpc_reference_site/passenger_cars_ng/mobile/mbp/new_cars/models/e-class/w213/11-2015/mercedes-benz-e-class-w213_modeloverview_814x383_11-2015_jpg.object-Single-MEDIA.tmp/mercedes-benz_e-class_w213_overview_814x383_10-2015.jpg",
//                "https://m.mercedes-benz.pl/content/media_library/hq/hq_mpc_reference_site/passenger_cars_ng/mobile/mbp/new_cars/models/cla-class/x117/sr_02-2016/mercedes-benz-cla-x117_modeloverview_814x383_02-2016_jpg.object-Single-MEDIA.tmp/mercedes-benz-cla_x117_modeloverview_814x383_02-2016.jpg",
//                "http://assets.autobuzz.my/wp-content/uploads/2018/02/14122050/2018-Mercedes-Benz-C-Class-Facelift-Unveiled-23-740x431.jpg",
//                "https://www.mercedes-benz.pl/content/media_library/hq/hq_mpc_reference_site/passenger_cars_ng/new_cars/models/mercedes-amg_gt/r190/start/mercedes-benz-amg-gt-r190_start_1000x470_10-2016_jpg.object-Single-MEDIA.tmp/mercedes-benz-amg-gt_r190_start_1000x470_10-2016.jpg",
//                "https://f2.blick.ch/img/incoming/origs5905123/3590141579-w980-h653/GT-R-2.jpg")
//        val porsche = arrayOf(
//                "https://files2.porsche.com/filestore/image/multimedia/none/ww-rangeshotcountryselector-front/normal/6496c099-1896-11e8-bbc5-0019999cd470;s5/porsche-normal.jpg",
//                "https://files1.porsche.com/filestore/image/multimedia/none/rd-2018-homepage-banner-ww-cayennee-hybrid-kw19/normal/a5d451f4-4a17-11e8-bbc5-0019999cd470/porsche-normal.jpg",
//                "https://files2.porsche.com/filestore/image/multimedia/none/panamera-exclusive-countryselector-13-5banner/normal/dcd99158-5aab-11e4-99aa-001a64c55f5c;s25/porsche-normal.jpg",
//                "http://www.porschepoznan.com.pl/images/model/menu/Nowe-Porsche-Panamera-Turbo-S-E-Hybrid.jpg")
        val colors = arrayOf(R.drawable.kolor1,R.drawable.kolor2,R.drawable.kolor3,
                R.drawable.kolor4,R.drawable.kolor5,R.drawable.kolor6,
                R.drawable.kolor7,R.drawable.kolor8,R.drawable.kolor9)

        val list = arrayListOf<Image>()

        for (index in colors.indices) {
            list.add(
                    buildMovieInfo(
                            "category",
                            colors[index]))
        }
        return list
    }

    private fun buildMovieInfo(category: String, cardImageUrl: Int): Image {
        val movie = Image()
        movie.id = count++

        movie.category = category
        movie.cardImageUrl = cardImageUrl
        return movie
    }
}