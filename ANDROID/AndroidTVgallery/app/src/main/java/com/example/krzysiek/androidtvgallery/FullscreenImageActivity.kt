package com.example.krzysiek.androidtvgallery

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.support.v4.app.NavUtils
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_fullscreen_image.*



class FullscreenImageActivity :  Activity() {
    private val mHideHandler = Handler()
    private val mHidePart2Runnable = Runnable {

        fullscreen_content.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val mShowPart2Runnable = Runnable {
        actionBar?.show()
        fullscreen_content_controls.visibility = View.VISIBLE
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    private val mDelayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen_image)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        mVisible = true
        fullscreen_content.setOnClickListener { toggle() }
        dummy_button.setOnTouchListener(mDelayHideTouchListener)
        val myIntent = this.intent
        image = (myIntent.getSerializableExtra(R.string.ExtraObject.toString()) as Image)
        changeImage()
    }
    fun changeImage(){
        findViewById<ImageView>(R.id.mainImage).setImageResource(image.cardImageUrl!!)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if ((keyCode == KeyEvent.KEYCODE_RIGHT_BRACKET || keyCode== KeyEvent.KEYCODE_DPAD_RIGHT) && event.repeatCount == 0) {
            var finded:Boolean=false
            for (i in ImageList.list)
            {
                if (finded){
                    if (i.category==image.category) {
                        image=i
                        changeImage()
                        break
                    }
                }
                if(i==image) {
                    finded=true;
                }
            }
            true
        } else
            if ((keyCode == KeyEvent.KEYCODE_LEFT_BRACKET || keyCode== KeyEvent.KEYCODE_DPAD_LEFT) && event.repeatCount == 0) {
                var previousIamge : Image=image
                for (i in ImageList.list)
                {
                    if (image == i){
                        image=previousIamge
                        changeImage()
                        break
                    }
                    previousIamge=i
                }
                true
            } else
                if ((keyCode == KeyEvent.KEYCODE_SYSTEM_NAVIGATION_DOWN || keyCode== KeyEvent.KEYCODE_DPAD_DOWN) && event.repeatCount == 0) {
                    super.onBackPressed();
                    true
                } else
                super.onKeyDown(keyCode, event)

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        actionBar?.hide()
        fullscreen_content_controls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {

        fullscreen_content.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true


        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        lateinit var image : Image ;

        private val AUTO_HIDE = true
        private val AUTO_HIDE_DELAY_MILLIS = 3000
        private val UI_ANIMATION_DELAY = 300
    }
}
