﻿using Biblioteka_Szkolna.View;
using System;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.Prezenter
{
    class KsiazkaOknoPrezenter
    {
        private readonly KsiazkaOkno okno;
        private readonly KsiazkaModel model;

        public KsiazkaOknoPrezenter(KsiazkaOkno okno)
        {
            this.okno = okno;
            model = new KsiazkaModel();
        }

        public KsiazkaModel OtworzIWybierz()
        {
            if (okno.ShowDialog() == DialogResult.Yes)
            {
                return model;
            }
            else return null;
        }

        public void OtworzOkno()
        {
            okno.Show();
        }
        public void TytulChange(string tytul)
        {
            model.Tytul = tytul.Trim();
        }
        public void AutorChange(string autor)
        {
            model.Autor.Nazwisko = autor.Trim();
        }
        public void WydawnictwoChange(string wydawnictwo)
        {
            model.Wydawnictwo.NazwaWydawnictwa = wydawnictwo.Trim();
        }
        public Boolean SprawdzDane()
        {
            if (model.PoprawneDaneFiltrowania() == true)
            {
                return true;
            }
            else
                return false;
        }
        public void DalejClick()
        {
            okno.DialogResult = DialogResult.Yes;
            okno.Close();
        }
    }
}
