﻿using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Prezenter
{
    public class CzytelnikListaKsiazekOknoPrezenter
    {
        private List<KsiazkaModel> model;
        private ICzytelnikListaKsiazekOkno okno;
        private Repozytorium repozytorium = new Repozytorium();
        public CzytelnikListaKsiazekOknoPrezenter(ICzytelnikListaKsiazekOkno okno)
        {
            this.okno = okno;
            okno.Prezenter = this;
            WczytajListe();
            okno.Aktualizuj();

        }

        public void WczytajListe(string tytul = "", string autor = "", string wydawnictwo = "")
        {
            model = repozytorium.DajListeKsiazek();
        }

        public void Szukaj()
        {
            model = repozytorium.DajListeKsiazek(okno.Tytul, okno.Autor, okno.Wydawnictwo);
            okno.Aktualizuj();
        }

        public List<KsiazkaModel> DajListe()
        {
            return model;
        }

        public void WyswietlKsiazkiCzytelnika(int idCzytelnika)
        {

        }

        public Boolean KsiazkaDostepna(int index)
        {
            
            return false;
        }
    }
}
