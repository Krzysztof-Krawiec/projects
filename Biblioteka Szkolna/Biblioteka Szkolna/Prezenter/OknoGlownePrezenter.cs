﻿using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.Prezenter;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.Presenter
{
    public class OknoGlownePrezenter
    {
        private readonly GlowneOkno okno;
        public OknoGlownePrezenter(GlowneOkno okno)
        {
            this.okno = okno;
            okno.Prezenter = this;
        }

        public void OnCzytelnicyClick()
        {
            ListaCzytelnikowOkno okno = new ListaCzytelnikowOkno();
            ListaCzytelnikowPrezenter prezenter = new ListaCzytelnikowPrezenter(okno);
            okno.Show();
        }

        public void OnWypozyczClick()
        {
            //WypozyczenieModel wypozyczenie = new WypozyczenieModel();
            //CzytelnikOkno okno = new CzytelnikOkno();
            WypozyczKsiazke wypozycz = new WypozyczKsiazke();
            wypozycz.Wypozycz();
        }
        public void OnKsiazkiClick()
        {
            CzytelnikListaKsiazekOkno okno = new CzytelnikListaKsiazekOkno();
            okno.ShowDialog();
        }
    }
}
