﻿using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.Prezenter
{
    public class ListaCzytelnikowPrezenter
    {
        private ListaCzytelnikowOkno okno;
        private List<CzytelnikModel> listaCzytelnikow;
        private int wybraneId =-1;
        private string imie = "", nazwisko = "", identyfikator = "";
        private Repozytorium repozytorium = new Repozytorium();
        public ListaCzytelnikowPrezenter(ListaCzytelnikowOkno okno)
        {
            this.okno = okno;
            okno.Prezenter = this;
            this.listaCzytelnikow = new List<CzytelnikModel>();
        }

        public int OtworzOknoIWybierz(Object obj)
        {
            imie = (obj as CzytelnikModel).Imie;
            nazwisko = (obj as CzytelnikModel).Nazwisko;
            identyfikator = (obj as CzytelnikModel).Identyfikator;
            okno.AktualizujListe();
            if (okno.ShowDialog() == DialogResult.Yes)
            {
                return wybraneId;
            }
            else
                return -1;
        }

        public List<CzytelnikModel> DajListe()
        {
            PobierzCzytelnicy();
            return listaCzytelnikow;
        }
        private void PobierzCzytelnicy()
        {
            listaCzytelnikow.Clear();
            listaCzytelnikow = repozytorium.DajListeCzytelnikow(imie, nazwisko, identyfikator);
        }
        public void WybierzClick(int position)
        {
            wybraneId = listaCzytelnikow[position].Id;
            okno.DialogResult = DialogResult.Yes;
            okno.Close();
        }

    }
}
