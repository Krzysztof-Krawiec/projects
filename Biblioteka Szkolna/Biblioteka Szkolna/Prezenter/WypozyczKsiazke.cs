﻿using Biblioteka_Szkolna.Kontekst;
using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Prezenter
{
    class WypozyczKsiazke
    {
        private Repozytorium repozytorium = new Repozytorium();
        public void Wypozycz()
        {
            WypozyczenieModel wypozyczenie = new WypozyczenieModel();
            wypozyczenie.Czytelnik = WybierzCzytelnika();
            if (wypozyczenie.Czytelnik == null)
            {
                System.Windows.Forms.MessageBox.Show("Nie znaleziono czytelnika o podanych danych");
                return;
            }
            if (!CzytelnikMozeWypozyczyc(wypozyczenie.Czytelnik.Id))
            {
                System.Windows.Forms.MessageBox.Show("Czytelnik ma wypożyczoną maksymalną liczbę egzemplarzy.");
                return;
            }

            wypozyczenie.Egzemplarz = WybierzEgzemplarz();
            if (wypozyczenie.Egzemplarz == null)
            {
                System.Windows.Forms.MessageBox.Show("Wybrana książka nie ma dostępnych egzemplarzy do wypożyczenia");
                return;
            }

            wypozyczenie.UstawDaty();
            repozytorium.DodajWypozyczenie(wypozyczenie);
            WypozyczenieOknoPrezenter w = new WypozyczenieOknoPrezenter(wypozyczenie);
        }
        public Boolean CzytelnikMozeWypozyczyc(int IdCzytelnika)
        {
            return repozytorium.DajLiczbeWypozyczonych(IdCzytelnika) <= 3;
        }
        public CzytelnikModel WybierzCzytelnika()
        {
            CzytelnikModel czytelnik = new CzytelnikModel();
            CzytelnikOkno okno = new CzytelnikOkno();
            if (okno.OtworzIWybierz(czytelnik))
            {
                ListaCzytelnikowOkno oknoCzytelnikow = new ListaCzytelnikowOkno();
                int id = oknoCzytelnikow.OtworzOknoIWybierz(czytelnik);
                if (id >= 0)
                {
                    return repozytorium.context.Czytelnicy.Where(x => x.Id == id).SingleOrDefault<CzytelnikModel>();
                }
                else return null;
            }
            else return null;
        }
        public EgzemplarzModel WybierzEgzemplarz()
        {
            EgzemplarzModel egzemplarz = new EgzemplarzModel();
            KsiazkaOkno okno = new KsiazkaOkno();
            KsiazkaModel ksiazka = (okno.OtworzIWybierz() as KsiazkaModel);
            if (ksiazka!= null)
            {
                ListaKsiazekOkno oknolistaKsiazek = new ListaKsiazekOkno();
                int id = oknolistaKsiazek.OtworzOknoIWybierz(ksiazka);
                id = repozytorium.context.Database.SqlQuery<int>("SELECT TOP(1) id_egzemplarz FROM Egzemplarz WHERE KsiazkaModel_id=@id AND id_egzemplarz NOT IN (SELECT Egzemplarz_Id FROM Wypozyczenie WHERE DataZwrotu is null OR DataZwrotu < DataWypozyczenia)", new SqlParameter("@id", id)).SingleOrDefault();
                return repozytorium.context.Egzemplarze.Where(c => c.Id == id).SingleOrDefault<EgzemplarzModel>();
            }
            return null;
        }
    }
}
