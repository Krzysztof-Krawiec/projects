﻿using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Prezenter
{
    public class WypozyczenieOknoPrezenter
    {
        private WypozyczenieOkno okno;
        private WypozyczenieModel model;
        private Repozytorium repozytorium = new Repozytorium();
        public WypozyczenieOknoPrezenter(WypozyczenieOkno okno)
        {
            this.okno = okno;
            okno.Prezenter = this;
            model = new WypozyczenieModel();
        }

        public WypozyczenieOknoPrezenter(WypozyczenieModel model)
        {
            this.model = model;
            this.okno = new WypozyczenieOkno();
            okno.Prezenter = this;
            Initialize();
            okno.ShowDialog();
        }
        private void Initialize()
        {
            try
            {
                okno.Tytul = repozytorium.DajTytulDlaEgzemplarza(model.Egzemplarz.Id);
                okno.Uzytkownik = repozytorium.context.Czytelnicy.Where(c => c.Id == model.Czytelnik.Id).SingleOrDefault().ToString();
                okno.TerminZwrotu = model.TerminZwrotu.ToString();
                okno.DataWypozyczenia = model.DataWypozyczenia.ToString();
            }
            catch 
            {
                
            }
        }

        public void ZamknijOkno()
        {
            okno.Close();
        }

    }
}
