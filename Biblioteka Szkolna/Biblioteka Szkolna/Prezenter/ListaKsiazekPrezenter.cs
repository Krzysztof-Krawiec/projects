﻿using Biblioteka_Szkolna.Kontekst;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.Prezenter
{
    public class ListaKsiazekPrezenter
    {
        private int WybraneId = -1;
        private ListaKsiazekOkno okno;
        private List<KsiazkaModel> listaKsiazek;
        private string tytul = "", wydawnictwo = "", autor = "";
        private Repozytorium repozytorium = new Repozytorium();
        public ListaKsiazekPrezenter(ListaKsiazekOkno okno)
        {
            this.okno = okno;
            okno.Prezenter = this;
            this.listaKsiazek = new List<KsiazkaModel>();
        }

        public int OtworzOknoIWybierz(Object obj)
        {
            tytul = (obj as KsiazkaModel).Tytul;
            wydawnictwo = (obj as KsiazkaModel)?.Wydawnictwo.NazwaWydawnictwa ?? "";
            autor = (obj as KsiazkaModel)?.Autor.Nazwisko ?? "";

            okno.AktualizujListe();
            if (okno.ShowDialog() == DialogResult.Yes)
            {
                return WybraneId;
            }
            else
                return -1;
        }

        public List<KsiazkaModel> DajListe()
        {
            PobierzKsiazki();
            return listaKsiazek;
        }
        private void PobierzKsiazki()
        {
            listaKsiazek.Clear();
            listaKsiazek = repozytorium.DajListeKsiazek(tytul, autor, wydawnictwo);
        }
        public void WybierzClick(int position)
        {
            WybraneId = listaKsiazek[position].Id;
            okno.DialogResult = DialogResult.Yes;
            okno.Close();
        }
    }
}
