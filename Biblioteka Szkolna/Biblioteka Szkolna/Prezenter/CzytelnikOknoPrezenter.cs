﻿using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.Prezenter
{

    public class CzytelnikOknoPrezenter {

        private readonly CzytelnikOkno okno;
        private readonly CzytelnikModel model;

        public CzytelnikOknoPrezenter(CzytelnikOkno okno)
        {
            this.okno = okno;
            model = new CzytelnikModel();
        }

        public Boolean OtworzIWybierz(Object obj)
        {
            if (okno.ShowDialog() == DialogResult.Yes)
            {
                (obj as CzytelnikModel).Imie = model.Imie;
                (obj as CzytelnikModel).Nazwisko = model.Nazwisko;
                (obj as CzytelnikModel).Identyfikator = model.Identyfikator;
                return true;
            }
            else return false;
        }

        public void OtworzOkno()
        {
            okno.Show();
        }
        public void ImieChanged(string imie)
        {
            model.Imie = imie.Trim();
        }
        public void NazwiskoChanged(string nazwisko)
        {
            model.Nazwisko = nazwisko.Trim();
        }
        public void IdentyfikatorChanged(string identyfikator)
        {
            model.Identyfikator = identyfikator.Trim();
        }
        public Boolean SprawdzDane()
        {
            if (model.PoprawneDaneFiltrowania() == true)
            {
                return true;
            }
            else
                return false;
        }
        public void DalejClick()
        {
            okno.DialogResult = DialogResult.Yes;
            okno.Close();
        }
    }


}
