﻿namespace Biblioteka_Szkolna.View
{
    partial class ListaKsiazekOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelKsiazki = new System.Windows.Forms.Label();
            this.listViewListaKsiazek = new System.Windows.Forms.ListView();
            this.columnHeaderTytul = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAutor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderWydawnictwo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonPowrot = new System.Windows.Forms.Button();
            this.buttonWybierz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelKsiazki
            // 
            this.labelKsiazki.AutoSize = true;
            this.labelKsiazki.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKsiazki.Location = new System.Drawing.Point(27, 30);
            this.labelKsiazki.Name = "labelKsiazki";
            this.labelKsiazki.Size = new System.Drawing.Size(215, 25);
            this.labelKsiazki.TabIndex = 17;
            this.labelKsiazki.Text = "Znaleziene książki:";
            this.labelKsiazki.UseMnemonic = false;
            // 
            // listViewListaKsiazek
            // 
            this.listViewListaKsiazek.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderTytul,
            this.columnHeaderAutor,
            this.columnHeaderWydawnictwo});
            this.listViewListaKsiazek.Location = new System.Drawing.Point(32, 111);
            this.listViewListaKsiazek.Name = "listViewListaKsiazek";
            this.listViewListaKsiazek.Size = new System.Drawing.Size(525, 245);
            this.listViewListaKsiazek.TabIndex = 16;
            this.listViewListaKsiazek.UseCompatibleStateImageBehavior = false;
            // 
            // columnHeaderTytul
            // 
            this.columnHeaderTytul.Text = "Tytuł";
            // 
            // columnHeaderAutor
            // 
            this.columnHeaderAutor.Text = "Autor";
            // 
            // columnHeaderWydawnictwo
            // 
            this.columnHeaderWydawnictwo.Text = "Wydawnictwo";
            // 
            // buttonPowrot
            // 
            this.buttonPowrot.Location = new System.Drawing.Point(34, 363);
            this.buttonPowrot.Name = "buttonPowrot";
            this.buttonPowrot.Size = new System.Drawing.Size(154, 46);
            this.buttonPowrot.TabIndex = 19;
            this.buttonPowrot.Text = "Powrót";
            this.buttonPowrot.UseVisualStyleBackColor = true;
            this.buttonPowrot.Click += new System.EventHandler(this.buttonPowrot_Click);
            // 
            // buttonWybierz
            // 
            this.buttonWybierz.Location = new System.Drawing.Point(403, 363);
            this.buttonWybierz.Name = "buttonWybierz";
            this.buttonWybierz.Size = new System.Drawing.Size(154, 46);
            this.buttonWybierz.TabIndex = 18;
            this.buttonWybierz.Text = "Wybierz";
            this.buttonWybierz.UseVisualStyleBackColor = true;
            this.buttonWybierz.Click += new System.EventHandler(this.buttonWybierz_Click_1);
            // 
            // ListaKsiazekOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 448);
            this.Controls.Add(this.labelKsiazki);
            this.Controls.Add(this.listViewListaKsiazek);
            this.Controls.Add(this.buttonPowrot);
            this.Controls.Add(this.buttonWybierz);
            this.Name = "ListaKsiazekOkno";
            this.Text = "Znalezione książki";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelKsiazki;
        private System.Windows.Forms.ListView listViewListaKsiazek;
        private System.Windows.Forms.ColumnHeader columnHeaderTytul;
        private System.Windows.Forms.Button buttonPowrot;
        private System.Windows.Forms.Button buttonWybierz;
        private System.Windows.Forms.ColumnHeader columnHeaderAutor;
        private System.Windows.Forms.ColumnHeader columnHeaderWydawnictwo;
    }
}