﻿namespace Biblioteka_Szkolna.View
{
    partial class CzytelnikListaKsiazekOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CzytelnikListaKsiazekOkno));
            this.buttonWypozyczoneKsiazki = new System.Windows.Forms.Button();
            this.buttonDostepnoscKsiazki = new System.Windows.Forms.Button();
            this.listViewListaKsiazek = new System.Windows.Forms.ListView();
            this.columnHeaderTytul = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAutor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderWydawnictwo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxAutor = new System.Windows.Forms.TextBox();
            this.textBoxWydawnictwo = new System.Windows.Forms.TextBox();
            this.labelTytul = new System.Windows.Forms.Label();
            this.textBoxTytul = new System.Windows.Forms.TextBox();
            this.labelWydawnictwo = new System.Windows.Forms.Label();
            this.labelAutor = new System.Windows.Forms.Label();
            this.buttonPowrot = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonWypozyczoneKsiazki
            // 
            this.buttonWypozyczoneKsiazki.Location = new System.Drawing.Point(21, 50);
            this.buttonWypozyczoneKsiazki.Name = "buttonWypozyczoneKsiazki";
            this.buttonWypozyczoneKsiazki.Size = new System.Drawing.Size(224, 38);
            this.buttonWypozyczoneKsiazki.TabIndex = 0;
            this.buttonWypozyczoneKsiazki.Text = "Wyświetl swoje wypożyczone książki";
            this.buttonWypozyczoneKsiazki.UseVisualStyleBackColor = true;
            this.buttonWypozyczoneKsiazki.Click += new System.EventHandler(this.buttonWypozyczoneKsiazki_Click);
            // 
            // buttonDostepnoscKsiazki
            // 
            this.buttonDostepnoscKsiazki.Location = new System.Drawing.Point(251, 50);
            this.buttonDostepnoscKsiazki.Name = "buttonDostepnoscKsiazki";
            this.buttonDostepnoscKsiazki.Size = new System.Drawing.Size(159, 38);
            this.buttonDostepnoscKsiazki.TabIndex = 1;
            this.buttonDostepnoscKsiazki.Text = "Sprawdź dostępność";
            this.buttonDostepnoscKsiazki.UseVisualStyleBackColor = true;
            this.buttonDostepnoscKsiazki.Click += new System.EventHandler(this.buttonDostepnoscKsiazki_Click);
            // 
            // listViewListaKsiazek
            // 
            this.listViewListaKsiazek.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderTytul,
            this.columnHeaderAutor,
            this.columnHeaderWydawnictwo});
            this.listViewListaKsiazek.Location = new System.Drawing.Point(21, 150);
            this.listViewListaKsiazek.Name = "listViewListaKsiazek";
            this.listViewListaKsiazek.Size = new System.Drawing.Size(754, 245);
            this.listViewListaKsiazek.TabIndex = 17;
            this.listViewListaKsiazek.UseCompatibleStateImageBehavior = false;
            // 
            // columnHeaderTytul
            // 
            this.columnHeaderTytul.Text = "Tytuł";
            // 
            // columnHeaderAutor
            // 
            this.columnHeaderAutor.Text = "Autor";
            // 
            // columnHeaderWydawnictwo
            // 
            this.columnHeaderWydawnictwo.Text = "Wydawnictwo";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Controls.Add(this.textBoxAutor);
            this.panel1.Controls.Add(this.textBoxWydawnictwo);
            this.panel1.Controls.Add(this.labelTytul);
            this.panel1.Controls.Add(this.textBoxTytul);
            this.panel1.Controls.Add(this.labelWydawnictwo);
            this.panel1.Controls.Add(this.labelAutor);
            this.panel1.Location = new System.Drawing.Point(21, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(754, 50);
            this.panel1.TabIndex = 18;
            // 
            // textBoxAutor
            // 
            this.textBoxAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAutor.Location = new System.Drawing.Point(271, 16);
            this.textBoxAutor.Name = "textBoxAutor";
            this.textBoxAutor.Size = new System.Drawing.Size(150, 26);
            this.textBoxAutor.TabIndex = 17;
            // 
            // textBoxWydawnictwo
            // 
            this.textBoxWydawnictwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxWydawnictwo.Location = new System.Drawing.Point(542, 16);
            this.textBoxWydawnictwo.Name = "textBoxWydawnictwo";
            this.textBoxWydawnictwo.Size = new System.Drawing.Size(150, 26);
            this.textBoxWydawnictwo.TabIndex = 16;
            // 
            // labelTytul
            // 
            this.labelTytul.AutoSize = true;
            this.labelTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTytul.Location = new System.Drawing.Point(4, 19);
            this.labelTytul.Name = "labelTytul";
            this.labelTytul.Size = new System.Drawing.Size(47, 20);
            this.labelTytul.TabIndex = 12;
            this.labelTytul.Text = "Tytuł:";
            // 
            // textBoxTytul
            // 
            this.textBoxTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTytul.Location = new System.Drawing.Point(57, 16);
            this.textBoxTytul.Name = "textBoxTytul";
            this.textBoxTytul.Size = new System.Drawing.Size(150, 26);
            this.textBoxTytul.TabIndex = 15;
            // 
            // labelWydawnictwo
            // 
            this.labelWydawnictwo.AutoSize = true;
            this.labelWydawnictwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWydawnictwo.Location = new System.Drawing.Point(427, 19);
            this.labelWydawnictwo.Name = "labelWydawnictwo";
            this.labelWydawnictwo.Size = new System.Drawing.Size(109, 20);
            this.labelWydawnictwo.TabIndex = 13;
            this.labelWydawnictwo.Text = "Wydawnictwo:";
            // 
            // labelAutor
            // 
            this.labelAutor.AutoSize = true;
            this.labelAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAutor.Location = new System.Drawing.Point(213, 19);
            this.labelAutor.Name = "labelAutor";
            this.labelAutor.Size = new System.Drawing.Size(52, 20);
            this.labelAutor.TabIndex = 14;
            this.labelAutor.Text = "Autor:";
            // 
            // buttonPowrot
            // 
            this.buttonPowrot.Location = new System.Drawing.Point(21, 401);
            this.buttonPowrot.Name = "buttonPowrot";
            this.buttonPowrot.Size = new System.Drawing.Size(154, 38);
            this.buttonPowrot.TabIndex = 19;
            this.buttonPowrot.Text = "Powrót";
            this.buttonPowrot.UseVisualStyleBackColor = true;
            this.buttonPowrot.Click += new System.EventHandler(this.buttonPowrot_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Image = ((System.Drawing.Image)(resources.GetObject("buttonSearch.Image")));
            this.buttonSearch.Location = new System.Drawing.Point(698, 3);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(45, 44);
            this.buttonSearch.TabIndex = 20;
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // CzytelnikListaKsiazekOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 478);
            this.Controls.Add(this.buttonPowrot);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listViewListaKsiazek);
            this.Controls.Add(this.buttonDostepnoscKsiazki);
            this.Controls.Add(this.buttonWypozyczoneKsiazki);
            this.Name = "CzytelnikListaKsiazekOkno";
            this.Text = "Lista książek";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonWypozyczoneKsiazki;
        private System.Windows.Forms.Button buttonDostepnoscKsiazki;
        private System.Windows.Forms.ListView listViewListaKsiazek;
        private System.Windows.Forms.ColumnHeader columnHeaderTytul;
        private System.Windows.Forms.ColumnHeader columnHeaderAutor;
        private System.Windows.Forms.ColumnHeader columnHeaderWydawnictwo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxAutor;
        private System.Windows.Forms.TextBox textBoxWydawnictwo;
        private System.Windows.Forms.Label labelTytul;
        private System.Windows.Forms.TextBox textBoxTytul;
        private System.Windows.Forms.Label labelWydawnictwo;
        private System.Windows.Forms.Label labelAutor;
        private System.Windows.Forms.Button buttonPowrot;
        private System.Windows.Forms.Button buttonSearch;
    }
}