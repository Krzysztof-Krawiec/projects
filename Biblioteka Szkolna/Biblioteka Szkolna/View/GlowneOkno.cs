﻿using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class GlowneOkno : Form
    {
        public OknoGlownePrezenter Prezenter;
        public GlowneOkno()
        {
            InitializeComponent();
        }

        private void buttonCzytelnicy_Click(object sender, EventArgs e)
        {
            Prezenter.OnCzytelnicyClick();
        }

        private void buttonWypozycz_Click(object sender, EventArgs e)
        {
            Prezenter.OnWypozyczClick();
        }

        private void buttonKsiazki_Click(object sender, EventArgs e)
        {
            Prezenter.OnKsiazkiClick();
        }
    }
}
