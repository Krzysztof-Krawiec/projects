﻿namespace Biblioteka_Szkolna.View
{
    partial class GlowneOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonEgzemplarze = new System.Windows.Forms.Button();
            this.buttonCzytelnicy = new System.Windows.Forms.Button();
            this.buttonKsiazki = new System.Windows.Forms.Button();
            this.labelPrzegladaj = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonZwroc = new System.Windows.Forms.Button();
            this.buttonWypozycz = new System.Windows.Forms.Button();
            this.labelWypozyczenia = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonEgzemplarze);
            this.panel1.Controls.Add(this.buttonCzytelnicy);
            this.panel1.Controls.Add(this.buttonKsiazki);
            this.panel1.Controls.Add(this.labelPrzegladaj);
            this.panel1.Location = new System.Drawing.Point(23, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(309, 265);
            this.panel1.TabIndex = 0;
            // 
            // buttonEgzemplarze
            // 
            this.buttonEgzemplarze.Location = new System.Drawing.Point(77, 195);
            this.buttonEgzemplarze.Name = "buttonEgzemplarze";
            this.buttonEgzemplarze.Size = new System.Drawing.Size(154, 46);
            this.buttonEgzemplarze.TabIndex = 15;
            this.buttonEgzemplarze.Text = "Egzemplarze";
            this.buttonEgzemplarze.UseVisualStyleBackColor = true;
            // 
            // buttonCzytelnicy
            // 
            this.buttonCzytelnicy.Location = new System.Drawing.Point(77, 143);
            this.buttonCzytelnicy.Name = "buttonCzytelnicy";
            this.buttonCzytelnicy.Size = new System.Drawing.Size(154, 46);
            this.buttonCzytelnicy.TabIndex = 14;
            this.buttonCzytelnicy.Text = "Czytelnicy";
            this.buttonCzytelnicy.UseVisualStyleBackColor = true;
            this.buttonCzytelnicy.Click += new System.EventHandler(this.buttonCzytelnicy_Click);
            // 
            // buttonKsiazki
            // 
            this.buttonKsiazki.Location = new System.Drawing.Point(77, 91);
            this.buttonKsiazki.Name = "buttonKsiazki";
            this.buttonKsiazki.Size = new System.Drawing.Size(154, 46);
            this.buttonKsiazki.TabIndex = 13;
            this.buttonKsiazki.Text = "Książki";
            this.buttonKsiazki.UseVisualStyleBackColor = true;
            this.buttonKsiazki.Click += new System.EventHandler(this.buttonKsiazki_Click);
            // 
            // labelPrzegladaj
            // 
            this.labelPrzegladaj.AutoSize = true;
            this.labelPrzegladaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPrzegladaj.Location = new System.Drawing.Point(89, 43);
            this.labelPrzegladaj.Name = "labelPrzegladaj";
            this.labelPrzegladaj.Size = new System.Drawing.Size(124, 25);
            this.labelPrzegladaj.TabIndex = 0;
            this.labelPrzegladaj.Text = "Przeglądaj";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonZwroc);
            this.panel2.Controls.Add(this.buttonWypozycz);
            this.panel2.Controls.Add(this.labelWypozyczenia);
            this.panel2.Location = new System.Drawing.Point(396, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(309, 265);
            this.panel2.TabIndex = 16;
            // 
            // buttonZwroc
            // 
            this.buttonZwroc.Location = new System.Drawing.Point(77, 143);
            this.buttonZwroc.Name = "buttonZwroc";
            this.buttonZwroc.Size = new System.Drawing.Size(154, 46);
            this.buttonZwroc.TabIndex = 14;
            this.buttonZwroc.Text = "Zwróc egzemplarz";
            this.buttonZwroc.UseVisualStyleBackColor = true;
            // 
            // buttonWypozycz
            // 
            this.buttonWypozycz.Location = new System.Drawing.Point(77, 91);
            this.buttonWypozycz.Name = "buttonWypozycz";
            this.buttonWypozycz.Size = new System.Drawing.Size(154, 46);
            this.buttonWypozycz.TabIndex = 13;
            this.buttonWypozycz.Text = "Wypożycz egzemplarz";
            this.buttonWypozycz.UseVisualStyleBackColor = true;
            this.buttonWypozycz.Click += new System.EventHandler(this.buttonWypozycz_Click);
            // 
            // labelWypozyczenia
            // 
            this.labelWypozyczenia.AutoSize = true;
            this.labelWypozyczenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWypozyczenia.Location = new System.Drawing.Point(72, 43);
            this.labelWypozyczenia.Name = "labelWypozyczenia";
            this.labelWypozyczenia.Size = new System.Drawing.Size(164, 25);
            this.labelWypozyczenia.TabIndex = 0;
            this.labelWypozyczenia.Text = "Wypożyczenia";
            // 
            // GlowneOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 398);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "GlowneOkno";
            this.Text = "Okno główne";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelPrzegladaj;
        private System.Windows.Forms.Button buttonEgzemplarze;
        private System.Windows.Forms.Button buttonCzytelnicy;
        private System.Windows.Forms.Button buttonKsiazki;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonZwroc;
        private System.Windows.Forms.Button buttonWypozycz;
        private System.Windows.Forms.Label labelWypozyczenia;
    }
}