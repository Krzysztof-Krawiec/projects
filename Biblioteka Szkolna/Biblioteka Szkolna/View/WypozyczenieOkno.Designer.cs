﻿namespace Biblioteka_Szkolna.View
{
    partial class WypozyczenieOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSzczegolyWypozyczenia = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelDatWyp = new System.Windows.Forms.Label();
            this.labelUzyt = new System.Windows.Forms.Label();
            this.labelWypEgzemplarz = new System.Windows.Forms.Label();
            this.labelDataWypozyczenia = new System.Windows.Forms.Label();
            this.labelUzytkownik = new System.Windows.Forms.Label();
            this.labelWypozyczonyEgzemplarz = new System.Windows.Forms.Label();
            this.labelTerminZwrotu = new System.Windows.Forms.Label();
            this.labelTermZwrot = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSzczegolyWypozyczenia
            // 
            this.labelSzczegolyWypozyczenia.AutoSize = true;
            this.labelSzczegolyWypozyczenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSzczegolyWypozyczenia.Location = new System.Drawing.Point(50, 71);
            this.labelSzczegolyWypozyczenia.Name = "labelSzczegolyWypozyczenia";
            this.labelSzczegolyWypozyczenia.Size = new System.Drawing.Size(274, 25);
            this.labelSzczegolyWypozyczenia.TabIndex = 6;
            this.labelSzczegolyWypozyczenia.Text = "Szczegóły wypożyczenia";
            this.labelSzczegolyWypozyczenia.UseMnemonic = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.labelTerminZwrotu);
            this.panel1.Controls.Add(this.labelTermZwrot);
            this.panel1.Controls.Add(this.labelDataWypozyczenia);
            this.panel1.Controls.Add(this.labelUzytkownik);
            this.panel1.Controls.Add(this.labelWypozyczonyEgzemplarz);
            this.panel1.Controls.Add(this.labelDatWyp);
            this.panel1.Controls.Add(this.labelUzyt);
            this.panel1.Controls.Add(this.labelWypEgzemplarz);
            this.panel1.Location = new System.Drawing.Point(55, 140);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 259);
            this.panel1.TabIndex = 7;
            // 
            // labelDatWyp
            // 
            this.labelDatWyp.AutoSize = true;
            this.labelDatWyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDatWyp.Location = new System.Drawing.Point(53, 136);
            this.labelDatWyp.Name = "labelDatWyp";
            this.labelDatWyp.Size = new System.Drawing.Size(149, 20);
            this.labelDatWyp.TabIndex = 11;
            this.labelDatWyp.Text = "Data wypożyczenia:";
            // 
            // labelUzyt
            // 
            this.labelUzyt.AutoSize = true;
            this.labelUzyt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelUzyt.Location = new System.Drawing.Point(53, 95);
            this.labelUzyt.Name = "labelUzyt";
            this.labelUzyt.Size = new System.Drawing.Size(93, 20);
            this.labelUzyt.TabIndex = 10;
            this.labelUzyt.Text = "Użytkownik:";
            // 
            // labelWypEgzemplarz
            // 
            this.labelWypEgzemplarz.AutoSize = true;
            this.labelWypEgzemplarz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWypEgzemplarz.Location = new System.Drawing.Point(53, 53);
            this.labelWypEgzemplarz.Name = "labelWypEgzemplarz";
            this.labelWypEgzemplarz.Size = new System.Drawing.Size(195, 20);
            this.labelWypEgzemplarz.TabIndex = 9;
            this.labelWypEgzemplarz.Text = "Wypożyczony egzemplarz:";
            // 
            // labelDataWypozyczenia
            // 
            this.labelDataWypozyczenia.AutoSize = true;
            this.labelDataWypozyczenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDataWypozyczenia.Location = new System.Drawing.Point(249, 136);
            this.labelDataWypozyczenia.Name = "labelDataWypozyczenia";
            this.labelDataWypozyczenia.Size = new System.Drawing.Size(0, 20);
            this.labelDataWypozyczenia.TabIndex = 14;
            // 
            // labelUzytkownik
            // 
            this.labelUzytkownik.AutoSize = true;
            this.labelUzytkownik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelUzytkownik.Location = new System.Drawing.Point(249, 95);
            this.labelUzytkownik.Name = "labelUzytkownik";
            this.labelUzytkownik.Size = new System.Drawing.Size(0, 20);
            this.labelUzytkownik.TabIndex = 13;
            // 
            // labelWypozyczonyEgzemplarz
            // 
            this.labelWypozyczonyEgzemplarz.AutoSize = true;
            this.labelWypozyczonyEgzemplarz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWypozyczonyEgzemplarz.Location = new System.Drawing.Point(249, 53);
            this.labelWypozyczonyEgzemplarz.Name = "labelWypozyczonyEgzemplarz";
            this.labelWypozyczonyEgzemplarz.Size = new System.Drawing.Size(0, 20);
            this.labelWypozyczonyEgzemplarz.TabIndex = 12;
            // 
            // labelTerminZwrotu
            // 
            this.labelTerminZwrotu.AutoSize = true;
            this.labelTerminZwrotu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTerminZwrotu.Location = new System.Drawing.Point(249, 173);
            this.labelTerminZwrotu.Name = "labelTerminZwrotu";
            this.labelTerminZwrotu.Size = new System.Drawing.Size(0, 20);
            this.labelTerminZwrotu.TabIndex = 16;
            // 
            // labelTermZwrot
            // 
            this.labelTermZwrot.AutoSize = true;
            this.labelTermZwrot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTermZwrot.Location = new System.Drawing.Point(53, 173);
            this.labelTermZwrot.Name = "labelTermZwrot";
            this.labelTermZwrot.Size = new System.Drawing.Size(112, 20);
            this.labelTermZwrot.TabIndex = 15;
            this.labelTermZwrot.Text = "Termin zwrotu:";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(532, 415);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(154, 46);
            this.buttonOk.TabIndex = 13;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // WypozyczenieOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 473);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelSzczegolyWypozyczenia);
            this.Name = "WypozyczenieOkno";
            this.Text = "Szczegóły wypożyczenia";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSzczegolyWypozyczenia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelTerminZwrotu;
        private System.Windows.Forms.Label labelTermZwrot;
        private System.Windows.Forms.Label labelDataWypozyczenia;
        private System.Windows.Forms.Label labelUzytkownik;
        private System.Windows.Forms.Label labelWypozyczonyEgzemplarz;
        private System.Windows.Forms.Label labelDatWyp;
        private System.Windows.Forms.Label labelUzyt;
        private System.Windows.Forms.Label labelWypEgzemplarz;
        private System.Windows.Forms.Button buttonOk;
    }
}