﻿namespace Biblioteka_Szkolna.View
{
    partial class KsiazkaOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPowrot = new System.Windows.Forms.Button();
            this.buttonDalej = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxAutor = new System.Windows.Forms.TextBox();
            this.textBoxWydawnictwo = new System.Windows.Forms.TextBox();
            this.textBoxTytul = new System.Windows.Forms.TextBox();
            this.labelAutor = new System.Windows.Forms.Label();
            this.labelWydawnictwo = new System.Windows.Forms.Label();
            this.labelTytul = new System.Windows.Forms.Label();
            this.labelDaneKsiazki = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPowrot
            // 
            this.buttonPowrot.Location = new System.Drawing.Point(22, 280);
            this.buttonPowrot.Name = "buttonPowrot";
            this.buttonPowrot.Size = new System.Drawing.Size(154, 46);
            this.buttonPowrot.TabIndex = 13;
            this.buttonPowrot.Text = "Powrót";
            this.buttonPowrot.UseVisualStyleBackColor = true;
            this.buttonPowrot.Click += new System.EventHandler(this.buttonPowrot_Click);
            // 
            // buttonDalej
            // 
            this.buttonDalej.Location = new System.Drawing.Point(260, 280);
            this.buttonDalej.Name = "buttonDalej";
            this.buttonDalej.Size = new System.Drawing.Size(154, 46);
            this.buttonDalej.TabIndex = 12;
            this.buttonDalej.Text = "Dalej";
            this.buttonDalej.UseVisualStyleBackColor = true;
            this.buttonDalej.Click += new System.EventHandler(this.buttonDalej_Click_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonPowrot);
            this.panel1.Controls.Add(this.buttonDalej);
            this.panel1.Controls.Add(this.textBoxAutor);
            this.panel1.Controls.Add(this.textBoxWydawnictwo);
            this.panel1.Controls.Add(this.textBoxTytul);
            this.panel1.Controls.Add(this.labelAutor);
            this.panel1.Controls.Add(this.labelWydawnictwo);
            this.panel1.Controls.Add(this.labelTytul);
            this.panel1.Controls.Add(this.labelDaneKsiazki);
            this.panel1.Location = new System.Drawing.Point(34, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 342);
            this.panel1.TabIndex = 1;
            // 
            // textBoxAutor
            // 
            this.textBoxAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAutor.Location = new System.Drawing.Point(175, 214);
            this.textBoxAutor.Name = "textBoxAutor";
            this.textBoxAutor.Size = new System.Drawing.Size(150, 26);
            this.textBoxAutor.TabIndex = 11;
            this.textBoxAutor.TextChanged += new System.EventHandler(this.textBoxAutor_TextChanged);
            // 
            // textBoxWydawnictwo
            // 
            this.textBoxWydawnictwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxWydawnictwo.Location = new System.Drawing.Point(175, 173);
            this.textBoxWydawnictwo.Name = "textBoxWydawnictwo";
            this.textBoxWydawnictwo.Size = new System.Drawing.Size(150, 26);
            this.textBoxWydawnictwo.TabIndex = 10;
            this.textBoxWydawnictwo.TextChanged += new System.EventHandler(this.textBoxWydawnictwo_TextChanged);
            // 
            // textBoxTytul
            // 
            this.textBoxTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTytul.Location = new System.Drawing.Point(175, 131);
            this.textBoxTytul.Name = "textBoxTytul";
            this.textBoxTytul.Size = new System.Drawing.Size(150, 26);
            this.textBoxTytul.TabIndex = 9;
            this.textBoxTytul.TextChanged += new System.EventHandler(this.textBoxTytul_TextChanged);
            // 
            // labelAutor
            // 
            this.labelAutor.AutoSize = true;
            this.labelAutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAutor.Location = new System.Drawing.Point(49, 217);
            this.labelAutor.Name = "labelAutor";
            this.labelAutor.Size = new System.Drawing.Size(52, 20);
            this.labelAutor.TabIndex = 8;
            this.labelAutor.Text = "Autor:";
            // 
            // labelWydawnictwo
            // 
            this.labelWydawnictwo.AutoSize = true;
            this.labelWydawnictwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWydawnictwo.Location = new System.Drawing.Point(49, 176);
            this.labelWydawnictwo.Name = "labelWydawnictwo";
            this.labelWydawnictwo.Size = new System.Drawing.Size(109, 20);
            this.labelWydawnictwo.TabIndex = 7;
            this.labelWydawnictwo.Text = "Wydawnictwo:";
            // 
            // labelTytul
            // 
            this.labelTytul.AutoSize = true;
            this.labelTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTytul.Location = new System.Drawing.Point(49, 134);
            this.labelTytul.Name = "labelTytul";
            this.labelTytul.Size = new System.Drawing.Size(47, 20);
            this.labelTytul.TabIndex = 6;
            this.labelTytul.Text = "Tytuł:";
            // 
            // labelDaneKsiazki
            // 
            this.labelDaneKsiazki.AutoSize = true;
            this.labelDaneKsiazki.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDaneKsiazki.Location = new System.Drawing.Point(145, 38);
            this.labelDaneKsiazki.Name = "labelDaneKsiazki";
            this.labelDaneKsiazki.Size = new System.Drawing.Size(150, 25);
            this.labelDaneKsiazki.TabIndex = 5;
            this.labelDaneKsiazki.Text = "Dane Książki";
            this.labelDaneKsiazki.UseMnemonic = false;
            // 
            // KsiazkaOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 401);
            this.Controls.Add(this.panel1);
            this.Name = "KsiazkaOkno";
            this.Text = "Wprowadź dane książki";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPowrot;
        private System.Windows.Forms.Button buttonDalej;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxAutor;
        private System.Windows.Forms.TextBox textBoxWydawnictwo;
        private System.Windows.Forms.TextBox textBoxTytul;
        private System.Windows.Forms.Label labelAutor;
        private System.Windows.Forms.Label labelWydawnictwo;
        private System.Windows.Forms.Label labelTytul;
        private System.Windows.Forms.Label labelDaneKsiazki;
    }
}