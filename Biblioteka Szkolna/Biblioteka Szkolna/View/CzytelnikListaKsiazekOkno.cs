﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class CzytelnikListaKsiazekOkno : Form, ICzytelnikListaKsiazekOkno
    {
        public CzytelnikListaKsiazekOknoPrezenter prezenter;

        public string Tytul { get => textBoxTytul.Text; set => textBoxTytul.Text = value; }
        public string Autor { get => textBoxAutor.Text; set => textBoxAutor.Text = value; }
        public string Wydawnictwo { get => textBoxWydawnictwo.Text; set => textBoxWydawnictwo.Text = value; }
        public CzytelnikListaKsiazekOknoPrezenter Prezenter { get => prezenter; set => prezenter=value; }

        public CzytelnikListaKsiazekOkno()
        {
            InitializeComponent();
            prezenter = new CzytelnikListaKsiazekOknoPrezenter(this);
            listViewListaKsiazek.View = System.Windows.Forms.View.Details;
            listViewListaKsiazek.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        public void Aktualizuj()
        {
            listViewListaKsiazek.Items.Clear();
            var lista = prezenter.DajListe();
            foreach (var item in lista)
            {
                string tytul = item.Tytul;
                string autor = item.Autor?.ToString() ?? "";
                string wydawnictwo = item.Wydawnictwo?.ToString() ?? "";
                listViewListaKsiazek.Items.Add(new ListViewItem(new String[] { tytul, autor, wydawnictwo }));
            }
        }
        private void buttonWypozyczoneKsiazki_Click(object sender, EventArgs e)
        {
            prezenter.WyswietlKsiazkiCzytelnika(5);
        }

        private void buttonDostepnoscKsiazki_Click(object sender, EventArgs e)
        {
            if (listViewListaKsiazek.SelectedItems.Count != 0)
            {
                if (prezenter.KsiazkaDostepna(listViewListaKsiazek.SelectedItems[0].Index))
                {
                    MessageBox.Show("Książka jest dostępna");
                }
                else
                    MessageBox.Show("Książka nie jest dostępna");
            }
            else MessageBox.Show("Proszę zaznaczyć jedną książkę");
            
        }

        private void buttonPowrot_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            prezenter.Szukaj();
        }

    }
}
