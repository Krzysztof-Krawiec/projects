﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.View
{
    public interface ICzytelnikListaKsiazekOkno
    {
        string Tytul { get; set; }
        string Autor { get; set; }
        string Wydawnictwo { get; set; }
        void Aktualizuj();
        CzytelnikListaKsiazekOknoPrezenter Prezenter { get; set; }
    }
}
