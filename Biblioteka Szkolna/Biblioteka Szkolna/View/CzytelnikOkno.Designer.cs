﻿namespace Biblioteka_Szkolna.View
{
    partial class CzytelnikOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonPowrot = new System.Windows.Forms.Button();
            this.buttonDalej = new System.Windows.Forms.Button();
            this.textBoxIdentyfikator = new System.Windows.Forms.TextBox();
            this.textBoxNazwisko = new System.Windows.Forms.TextBox();
            this.textBoxImie = new System.Windows.Forms.TextBox();
            this.labelIdentyfikator = new System.Windows.Forms.Label();
            this.labelNazwisko = new System.Windows.Forms.Label();
            this.labelImie = new System.Windows.Forms.Label();
            this.labelDaneCzytelnika = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonPowrot);
            this.panel1.Controls.Add(this.buttonDalej);
            this.panel1.Controls.Add(this.textBoxIdentyfikator);
            this.panel1.Controls.Add(this.textBoxNazwisko);
            this.panel1.Controls.Add(this.textBoxImie);
            this.panel1.Controls.Add(this.labelIdentyfikator);
            this.panel1.Controls.Add(this.labelNazwisko);
            this.panel1.Controls.Add(this.labelImie);
            this.panel1.Controls.Add(this.labelDaneCzytelnika);
            this.panel1.Location = new System.Drawing.Point(47, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 342);
            this.panel1.TabIndex = 0;
            // 
            // buttonPowrot
            // 
            this.buttonPowrot.Location = new System.Drawing.Point(22, 280);
            this.buttonPowrot.Name = "buttonPowrot";
            this.buttonPowrot.Size = new System.Drawing.Size(154, 46);
            this.buttonPowrot.TabIndex = 13;
            this.buttonPowrot.Text = "Powrót";
            this.buttonPowrot.UseVisualStyleBackColor = true;
            this.buttonPowrot.Click += new System.EventHandler(this.buttonPowrot_Click);
            // 
            // buttonDalej
            // 
            this.buttonDalej.Location = new System.Drawing.Point(260, 280);
            this.buttonDalej.Name = "buttonDalej";
            this.buttonDalej.Size = new System.Drawing.Size(154, 46);
            this.buttonDalej.TabIndex = 12;
            this.buttonDalej.Text = "Dalej";
            this.buttonDalej.UseVisualStyleBackColor = true;
            this.buttonDalej.Click += new System.EventHandler(this.buttonDalej_Click);
            // 
            // textBoxIdentyfikator
            // 
            this.textBoxIdentyfikator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxIdentyfikator.Location = new System.Drawing.Point(175, 214);
            this.textBoxIdentyfikator.Name = "textBoxIdentyfikator";
            this.textBoxIdentyfikator.Size = new System.Drawing.Size(150, 26);
            this.textBoxIdentyfikator.TabIndex = 11;
            this.textBoxIdentyfikator.TextChanged += new System.EventHandler(this.textBoxIdentyfikator_TextChanged);
            // 
            // textBoxNazwisko
            // 
            this.textBoxNazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNazwisko.Location = new System.Drawing.Point(175, 173);
            this.textBoxNazwisko.Name = "textBoxNazwisko";
            this.textBoxNazwisko.Size = new System.Drawing.Size(150, 26);
            this.textBoxNazwisko.TabIndex = 10;
            this.textBoxNazwisko.TextChanged += new System.EventHandler(this.textBoxNazwisko_TextChanged);
            // 
            // textBoxImie
            // 
            this.textBoxImie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxImie.Location = new System.Drawing.Point(175, 131);
            this.textBoxImie.Name = "textBoxImie";
            this.textBoxImie.Size = new System.Drawing.Size(150, 26);
            this.textBoxImie.TabIndex = 9;
            this.textBoxImie.TextChanged += new System.EventHandler(this.textBoxImie_TextChanged);
            // 
            // labelIdentyfikator
            // 
            this.labelIdentyfikator.AutoSize = true;
            this.labelIdentyfikator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelIdentyfikator.Location = new System.Drawing.Point(49, 217);
            this.labelIdentyfikator.Name = "labelIdentyfikator";
            this.labelIdentyfikator.Size = new System.Drawing.Size(101, 20);
            this.labelIdentyfikator.TabIndex = 8;
            this.labelIdentyfikator.Text = "Identyfikator:";
            // 
            // labelNazwisko
            // 
            this.labelNazwisko.AutoSize = true;
            this.labelNazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNazwisko.Location = new System.Drawing.Point(49, 176);
            this.labelNazwisko.Name = "labelNazwisko";
            this.labelNazwisko.Size = new System.Drawing.Size(80, 20);
            this.labelNazwisko.TabIndex = 7;
            this.labelNazwisko.Text = "Nazwisko:";
            // 
            // labelImie
            // 
            this.labelImie.AutoSize = true;
            this.labelImie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelImie.Location = new System.Drawing.Point(49, 134);
            this.labelImie.Name = "labelImie";
            this.labelImie.Size = new System.Drawing.Size(43, 20);
            this.labelImie.TabIndex = 6;
            this.labelImie.Text = "Imię:";
            // 
            // labelDaneCzytelnika
            // 
            this.labelDaneCzytelnika.AutoSize = true;
            this.labelDaneCzytelnika.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDaneCzytelnika.Location = new System.Drawing.Point(145, 38);
            this.labelDaneCzytelnika.Name = "labelDaneCzytelnika";
            this.labelDaneCzytelnika.Size = new System.Drawing.Size(180, 25);
            this.labelDaneCzytelnika.TabIndex = 5;
            this.labelDaneCzytelnika.Text = "Dane czytelnika";
            this.labelDaneCzytelnika.UseMnemonic = false;
            // 
            // CzytelnikOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 420);
            this.Controls.Add(this.panel1);
            this.Name = "CzytelnikOkno";
            this.Text = "Wprowadź dane czytelnika";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxIdentyfikator;
        private System.Windows.Forms.TextBox textBoxNazwisko;
        private System.Windows.Forms.TextBox textBoxImie;
        private System.Windows.Forms.Label labelIdentyfikator;
        private System.Windows.Forms.Label labelNazwisko;
        private System.Windows.Forms.Label labelImie;
        private System.Windows.Forms.Label labelDaneCzytelnika;
        private System.Windows.Forms.Button buttonPowrot;
        private System.Windows.Forms.Button buttonDalej;
    }
}