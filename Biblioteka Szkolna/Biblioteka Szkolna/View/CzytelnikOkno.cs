﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class CzytelnikOkno : Form
    {
        private CzytelnikOknoPrezenter Prezenter { get; set; }
        public CzytelnikOkno()
        {
            InitializeComponent();
            this.Prezenter = new CzytelnikOknoPrezenter(this);
        }
        public Boolean OtworzIWybierz(Object obj)
        {
            return Prezenter.OtworzIWybierz(obj);
        }
        private void textBoxImie_TextChanged(object sender, EventArgs e)
        {
            Prezenter.ImieChanged(textBoxImie.Text);
        }

        private void textBoxNazwisko_TextChanged(object sender, EventArgs e)
        {
            Prezenter.NazwiskoChanged(textBoxNazwisko.Text);
        }

        private void textBoxIdentyfikator_TextChanged(object sender, EventArgs e)
        {
            Prezenter.IdentyfikatorChanged(textBoxIdentyfikator.Text);
        }

        private void buttonDalej_Click(object sender, EventArgs e)
        {
            if (Prezenter.SprawdzDane())
            {
                Prezenter.DalejClick();
            }
            else
            {
                MessageBox.Show("Proszę wypełnić co najmniej jedno pole");
            }
        }

        private void buttonPowrot_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
