﻿namespace Biblioteka_Szkolna.View
{
    partial class ListaCzytelnikowOkno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewListaCzytelnikow = new System.Windows.Forms.ListView();
            this.Imie = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nazwisko = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelWypozyczenia = new System.Windows.Forms.Label();
            this.buttonPowrot = new System.Windows.Forms.Button();
            this.buttonWybierz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewListaCzytelnikow
            // 
            this.listViewListaCzytelnikow.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Imie,
            this.Nazwisko});
            this.listViewListaCzytelnikow.Location = new System.Drawing.Point(49, 125);
            this.listViewListaCzytelnikow.Name = "listViewListaCzytelnikow";
            this.listViewListaCzytelnikow.Size = new System.Drawing.Size(525, 245);
            this.listViewListaCzytelnikow.TabIndex = 0;
            this.listViewListaCzytelnikow.UseCompatibleStateImageBehavior = false;
            // 
            // Imie
            // 
            this.Imie.Text = "Imię";
            this.Imie.Width = 120;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Text = "Nazwisko";
            this.Nazwisko.Width = 120;
            // 
            // labelWypozyczenia
            // 
            this.labelWypozyczenia.AutoSize = true;
            this.labelWypozyczenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWypozyczenia.Location = new System.Drawing.Point(44, 43);
            this.labelWypozyczenia.Name = "labelWypozyczenia";
            this.labelWypozyczenia.Size = new System.Drawing.Size(240, 25);
            this.labelWypozyczenia.TabIndex = 5;
            this.labelWypozyczenia.Text = "Znalezieni czytelnicy:";
            this.labelWypozyczenia.UseMnemonic = false;
            // 
            // buttonPowrot
            // 
            this.buttonPowrot.Location = new System.Drawing.Point(51, 376);
            this.buttonPowrot.Name = "buttonPowrot";
            this.buttonPowrot.Size = new System.Drawing.Size(154, 46);
            this.buttonPowrot.TabIndex = 15;
            this.buttonPowrot.Text = "Powrót";
            this.buttonPowrot.UseVisualStyleBackColor = true;
            this.buttonPowrot.Click += new System.EventHandler(this.buttonPowrot_Click);
            // 
            // buttonWybierz
            // 
            this.buttonWybierz.Location = new System.Drawing.Point(420, 376);
            this.buttonWybierz.Name = "buttonWybierz";
            this.buttonWybierz.Size = new System.Drawing.Size(154, 46);
            this.buttonWybierz.TabIndex = 14;
            this.buttonWybierz.Text = "Wybierz";
            this.buttonWybierz.UseVisualStyleBackColor = true;
            this.buttonWybierz.Click += new System.EventHandler(this.buttonWybierz_Click);
            // 
            // ListaCzytelnikowOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 462);
            this.Controls.Add(this.buttonPowrot);
            this.Controls.Add(this.buttonWybierz);
            this.Controls.Add(this.labelWypozyczenia);
            this.Controls.Add(this.listViewListaCzytelnikow);
            this.Name = "ListaCzytelnikowOkno";
            this.Text = "Znalezieni czytelnicy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewListaCzytelnikow;
        private System.Windows.Forms.ColumnHeader Imie;
        private System.Windows.Forms.Label labelWypozyczenia;
        private System.Windows.Forms.Button buttonPowrot;
        private System.Windows.Forms.Button buttonWybierz;
        private System.Windows.Forms.ColumnHeader Nazwisko;
    }
}