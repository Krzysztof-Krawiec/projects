﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class ListaKsiazekOkno : Form
    {
        public ListaKsiazekPrezenter Prezenter { get; set; }

        public ListaKsiazekOkno()
        {
            InitializeComponent();
            Prezenter = new ListaKsiazekPrezenter(this);
            listViewListaKsiazek.View = System.Windows.Forms.View.Details;
            listViewListaKsiazek.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            AktualizujListe();
        }
        public int OtworzOknoIWybierz(Object obj)
        {
            return Prezenter.OtworzOknoIWybierz(obj);
        }
        public void AktualizujListe()
        {
            listViewListaKsiazek.Items.Clear();
            var lista = Prezenter.DajListe();
            foreach (var item in lista)
            {
                string tytul = item.Tytul;
                string autor = item.Autor?.ToString() ?? "";
                string wydawnictwo = item.Wydawnictwo?.ToString() ?? "";
                listViewListaKsiazek.Items.Add(new ListViewItem(new String[] { tytul, autor, wydawnictwo}));
            }
        }

        private void buttonWybierz_Click_1(object sender, EventArgs e)
        {
            Prezenter.WybierzClick(listViewListaKsiazek.SelectedItems[0].Index);
        }

        private void buttonPowrot_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
