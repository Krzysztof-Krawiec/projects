﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.View
{
    interface IWypozyczenieView
    {
        string Tytul { get; set; }
        string Uzytkownik { get; set; }
        string DataWypozyczenia { get; set; }
        string TerminZwrotu { get; set; }
        void ZamknijOkno();
    }
}
