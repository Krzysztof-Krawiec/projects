﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class KsiazkaOkno : Form
    {
        private KsiazkaOknoPrezenter Prezenter { get; set; }

        public KsiazkaOkno()
        {
            InitializeComponent();
            this.Prezenter = new KsiazkaOknoPrezenter(this);
        }
        public Object OtworzIWybierz()
        {
            return Prezenter.OtworzIWybierz();
        }
        
        private void buttonDalej_Click(object sender, EventArgs e)
        {
            if (Prezenter.SprawdzDane())
            {
                Prezenter.DalejClick();
            }
            else
            {
                MessageBox.Show("Proszę wypełnić co najmniej jedno pole");
            }
        }

        private void textBoxAutor_TextChanged(object sender, EventArgs e)
        {
            Prezenter.AutorChange(textBoxAutor.Text);
        }

        private void textBoxTytul_TextChanged(object sender, EventArgs e)
        {
            Prezenter.TytulChange(textBoxTytul.Text);
        }

        private void textBoxWydawnictwo_TextChanged(object sender, EventArgs e)
        {
            Prezenter.WydawnictwoChange(textBoxWydawnictwo.Text);
        }

        private void buttonDalej_Click_1(object sender, EventArgs e)
        {
            Prezenter.DalejClick();
        }

        private void buttonPowrot_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
