﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class WypozyczenieOkno : Form, IWypozyczenieView
    {
        public WypozyczenieOknoPrezenter Prezenter { get; set; }
        public WypozyczenieOkno()
        {
            InitializeComponent();
            this.Prezenter = new WypozyczenieOknoPrezenter(this);
        }

        public string Tytul { get => labelWypozyczonyEgzemplarz.Text; set => labelWypozyczonyEgzemplarz.Text = value; }
        public string Uzytkownik { get => labelUzytkownik.Text; set => labelUzytkownik.Text = value; }
        public string DataWypozyczenia { get => labelDataWypozyczenia.Text; set => labelDataWypozyczenia.Text = value; }
        public string TerminZwrotu { get => labelTerminZwrotu.Text; set => labelTerminZwrotu.Text = value; }

        public void ZamknijOkno()
        {
            Prezenter.ZamknijOkno();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            ZamknijOkno();
        }
    }
}
