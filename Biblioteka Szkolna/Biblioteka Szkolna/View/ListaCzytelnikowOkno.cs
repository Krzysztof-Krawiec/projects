﻿using Biblioteka_Szkolna.Prezenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna.View
{
    public partial class ListaCzytelnikowOkno : Form
    {
        public ListaCzytelnikowPrezenter Prezenter { get; set; }
        
        public ListaCzytelnikowOkno()
        {
            InitializeComponent();
            Prezenter = new ListaCzytelnikowPrezenter(this);
            listViewListaCzytelnikow.View = System.Windows.Forms.View.Details;
            listViewListaCzytelnikow.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            AktualizujListe();
        }
        public int OtworzOknoIWybierz(Object obj)
        {
            return Prezenter.OtworzOknoIWybierz(obj);
        }
        public void AktualizujListe()
        {
            listViewListaCzytelnikow.Items.Clear();
            var lista = Prezenter.DajListe();
            foreach (var item in lista)
            {
                listViewListaCzytelnikow.Items.Add(new ListViewItem(new String[] { item.Imie, item.Nazwisko }));
            }
        }

        private void buttonWybierz_Click(object sender, EventArgs e)
        {
            if (listViewListaCzytelnikow.SelectedItems[0] != null)
                Prezenter.WybierzClick(listViewListaCzytelnikow.SelectedItems[0].Index);
            else
                MessageBox.Show("Proszę wybrać jednego czytelnika");
        }
        public void WyswietlWiadomosc(string Wiadomosc)
        {
            MessageBox.Show(Wiadomosc);
        }

        private void buttonPowrot_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
