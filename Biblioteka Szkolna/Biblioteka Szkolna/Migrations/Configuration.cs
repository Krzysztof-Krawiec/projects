namespace Biblioteka_Szkolna.Migrations
{
    using Biblioteka_Szkolna.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Biblioteka_Szkolna.Kontekst.BibliotekaKontekst>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Biblioteka_Szkolna.Kontekst.BibliotekaKontekst context)
        {
            WydawnictwoModel wydawnictwoPwn = new WydawnictwoModel("PWN");
            WydawnictwoModel wydawnictwoGreg = new WydawnictwoModel("Greg");
            context.Wydawnictwa.AddOrUpdate(x => x.Id, wydawnictwoPwn, wydawnictwoGreg);

            AutorModel autorSienkiewicz = new AutorModel("Henryk", "Sienkiewicz");
            AutorModel autorMickiewicz = new AutorModel("Adam", "Mickiewicz");
            AutorModel autorCamus = new AutorModel("Albert", "Camus");
            AutorModel autorSlowacki = new AutorModel("Juliusz", "S�owacki");
            AutorModel autorSzekspir = new AutorModel("William", "Szekspir");
            context.Autorzy.AddOrUpdate(x => x.Id, autorSienkiewicz, autorMickiewicz);
            context.Ksiazki.AddOrUpdate(x => x.Id,
                        new KsiazkaModel("Ogniem i mieczem", "978-83-65875-73-0", "XVII wiek, na Kresach Wschodnich zbliza sie wojna pomiedzy Rzeczpospolita a Kozakami. Jan Skrzetuski, zolnierz choragwi ksiecia Jeremiego Wisniowieckiego, wraca z Krymu i przypadkiem uwalnia z rak opryszkow dowodce Kozakow, Bohdana Chmielnickiego.", autorSienkiewicz, wydawnictwoPwn),
                        new KsiazkaModel("Dziady cz. I", "978-83-65875-73-0", autorSienkiewicz, wydawnictwoPwn),
                        new KsiazkaModel("Dziady cz. II", "971-83-65875-73-0", autorSienkiewicz, wydawnictwoPwn),
                        new KsiazkaModel("Dziady cz. III", "974-83-65875-73-0", autorSienkiewicz, wydawnictwoPwn),
                        new KsiazkaModel("Dziady cz. IV", "973-83-65875-73-0", autorSienkiewicz, wydawnictwoPwn),
                        new KsiazkaModel("Kordian", "973-81-65875-73-0", autorSlowacki, wydawnictwoGreg),
                        new KsiazkaModel("Balladyna", "973-81-65875-73-1", autorSlowacki, wydawnictwoGreg),
                        new KsiazkaModel("Gr�b Agamemnona", "973-81-65875-31-0", autorSlowacki, wydawnictwoPwn),
                        new KsiazkaModel("Powr�t pos�a", "243-81-65875-31-0", autorSlowacki, wydawnictwoGreg),
                        new KsiazkaModel("Hamlet", "973-81-13275-73-0", autorSzekspir, wydawnictwoGreg),
                        new KsiazkaModel("Romeo i Julia", "973-41-13275-73-0", autorSzekspir, wydawnictwoPwn));

            context.Czytelnicy.AddOrUpdate(x => x.Id, new CzytelnikModel("Adam", "Nowak", "0001/2018"), new CzytelnikModel("Jan", "Kowalski", "0002/2018"), 
                                                      new CzytelnikModel("Sebastian", "Musia�", "0003/2018"), new CzytelnikModel("Paulina", "Tomczyk", "0004/2018"),
                                                      new CzytelnikModel("Marta", "Urba�ska", "0005/2018"), new CzytelnikModel("Karolina", "Szymczak", "0006/2018"));
            context.Bibliotekarze.AddOrUpdate(x => x.Id, new BibliotekarzModel("Krystyna", "Abacka", "krysta", "123"), new BibliotekarzModel("Krzysztof", "Bednarek", "kk", "kk123"));

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
