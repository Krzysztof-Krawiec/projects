namespace Biblioteka_Szkolna.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Autor",
                c => new
                    {
                        id_autor = c.Int(nullable: false, identity: true),
                        Biografia = c.String(),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        DataUrodzenia = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id_autor);
            
            CreateTable(
                "dbo.Ksiazka",
                c => new
                    {
                        id_ksiazka = c.Int(nullable: false, identity: true),
                        Tytul = c.String(),
                        Isbn = c.String(),
                        Opis = c.String(),
                        Autor_Id = c.Int(),
                        Wydawnictwo_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id_ksiazka)
                .ForeignKey("dbo.Autor", t => t.Autor_Id)
                .ForeignKey("dbo.Wydawnictwo", t => t.Wydawnictwo_Id)
                .Index(t => t.Autor_Id)
                .Index(t => t.Wydawnictwo_Id);
            
            CreateTable(
                "dbo.Egzemplarz",
                c => new
                    {
                        id_egzemplarz = c.Int(nullable: false, identity: true),
                        NumerEgzemplarza = c.String(),
                        IloscStron = c.Int(nullable: false),
                        KsiazkaModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id_egzemplarz)
                .ForeignKey("dbo.Ksiazka", t => t.KsiazkaModel_Id)
                .Index(t => t.KsiazkaModel_Id);
            
            CreateTable(
                "dbo.Wydawnictwo",
                c => new
                    {
                        id_wydawnictwo = c.Int(nullable: false, identity: true),
                        NazwaWydawnictwa = c.String(),
                    })
                .PrimaryKey(t => t.id_wydawnictwo);
            
            CreateTable(
                "dbo.Bibliotekarz",
                c => new
                    {
                        id_bibliotekarz = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Haslo = c.String(),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        DataUrodzenia = c.DateTime(nullable: false),
                        AdministratorModel_ID = c.Int(),
                    })
                .PrimaryKey(t => t.id_bibliotekarz)
                .ForeignKey("dbo.Administrator", t => t.AdministratorModel_ID)
                .Index(t => t.AdministratorModel_ID);
            
            CreateTable(
                "dbo.Czytelnik",
                c => new
                    {
                        id_czytelnik = c.Int(nullable: false, identity: true),
                        Identyfikator = c.String(),
                        Login = c.String(),
                        Haslo = c.String(),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        DataUrodzenia = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id_czytelnik);
            
            CreateTable(
                "dbo.Wypozyczenie",
                c => new
                    {
                        id_wypozyczenie = c.Int(nullable: false, identity: true),
                        DataWypozyczenia = c.DateTime(nullable: false),
                        TerminZwrotu = c.DateTime(nullable: false),
                        DataZwrotu = c.DateTime(nullable: false),
                        Bibliotekarz_Id = c.Int(),
                        Czytelnik_Id = c.Int(),
                        Egzemplarz_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id_wypozyczenie)
                .ForeignKey("dbo.Bibliotekarz", t => t.Bibliotekarz_Id)
                .ForeignKey("dbo.Czytelnik", t => t.Czytelnik_Id)
                .ForeignKey("dbo.Egzemplarz", t => t.Egzemplarz_Id)
                .Index(t => t.Bibliotekarz_Id)
                .Index(t => t.Czytelnik_Id)
                .Index(t => t.Egzemplarz_Id);
            
            CreateTable(
                "dbo.Administrator",
                c => new
                    {
                        id_administrator = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Haslo = c.String(),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        DataUrodzenia = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id_administrator);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bibliotekarz", "AdministratorModel_ID", "dbo.Administrator");
            DropForeignKey("dbo.Wypozyczenie", "Egzemplarz_Id", "dbo.Egzemplarz");
            DropForeignKey("dbo.Wypozyczenie", "Czytelnik_Id", "dbo.Czytelnik");
            DropForeignKey("dbo.Wypozyczenie", "Bibliotekarz_Id", "dbo.Bibliotekarz");
            DropForeignKey("dbo.Ksiazka", "Wydawnictwo_Id", "dbo.Wydawnictwo");
            DropForeignKey("dbo.Egzemplarz", "KsiazkaModel_Id", "dbo.Ksiazka");
            DropForeignKey("dbo.Ksiazka", "Autor_Id", "dbo.Autor");
            DropIndex("dbo.Wypozyczenie", new[] { "Egzemplarz_Id" });
            DropIndex("dbo.Wypozyczenie", new[] { "Czytelnik_Id" });
            DropIndex("dbo.Wypozyczenie", new[] { "Bibliotekarz_Id" });
            DropIndex("dbo.Bibliotekarz", new[] { "AdministratorModel_ID" });
            DropIndex("dbo.Egzemplarz", new[] { "KsiazkaModel_Id" });
            DropIndex("dbo.Ksiazka", new[] { "Wydawnictwo_Id" });
            DropIndex("dbo.Ksiazka", new[] { "Autor_Id" });
            DropTable("dbo.Administrator");
            DropTable("dbo.Wypozyczenie");
            DropTable("dbo.Czytelnik");
            DropTable("dbo.Bibliotekarz");
            DropTable("dbo.Wydawnictwo");
            DropTable("dbo.Egzemplarz");
            DropTable("dbo.Ksiazka");
            DropTable("dbo.Autor");
        }
    }
}
