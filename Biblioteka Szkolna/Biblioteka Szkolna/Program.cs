﻿using Biblioteka_Szkolna.Kontekst;
using Biblioteka_Szkolna.Model;
using Biblioteka_Szkolna.Presenter;
using Biblioteka_Szkolna.Prezenter;
using Biblioteka_Szkolna.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka_Szkolna
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            GlowneOkno okno = new GlowneOkno();
            OknoGlownePrezenter prezenter = new OknoGlownePrezenter(okno);
            
            Application.Run(okno);
        }
    }
}
