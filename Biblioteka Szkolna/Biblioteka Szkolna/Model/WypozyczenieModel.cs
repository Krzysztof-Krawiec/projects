﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Wypozyczenie")]
    public class WypozyczenieModel
    {
        [Key]
        [Column("id_wypozyczenie")]
        public int Id { get; set; }
        public DateTime DataWypozyczenia { get; set; }
        public DateTime TerminZwrotu { get; set; }
        public DateTime DataZwrotu { get; set; }
        [NotMapped]
        public Boolean Przeterminowane { get; set; }
        public EgzemplarzModel Egzemplarz { get; set; }
        public CzytelnikModel Czytelnik { get; set; }
        public BibliotekarzModel Bibliotekarz { get; set; }

        public WypozyczenieModel()
        {
            Czytelnik = new CzytelnikModel();
        }

        public WypozyczenieModel(DateTime dataWypozyczenia, DateTime terminZwrotu)
        {
            DataWypozyczenia = dataWypozyczenia;
            TerminZwrotu = terminZwrotu;
        }
        public void UstawDaty ()
        {
            DataWypozyczenia = DateTime.Now;
            TerminZwrotu = DataWypozyczenia.AddDays(21);
        }



    }
}
