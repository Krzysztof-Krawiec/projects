﻿using Biblioteka_Szkolna.Kontekst;
using Biblioteka_Szkolna.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Prezenter
{
    class Repozytorium
    {
        public BibliotekaKontekst context = new BibliotekaKontekst();

        public Boolean DodajWypozyczenie(WypozyczenieModel wypozyczenie)
        {
            context.Wypozyczenia.Add(wypozyczenie);
            if (wypozyczenie.Bibliotekarz != null)
                context.Entry(wypozyczenie.Bibliotekarz).State = EntityState.Unchanged;
            if (wypozyczenie.Egzemplarz != null)
                context.Entry(wypozyczenie.Egzemplarz).State = EntityState.Unchanged;
            if (wypozyczenie.Czytelnik != null)
                context.Entry(wypozyczenie.Czytelnik).State = EntityState.Unchanged;
            if (context.SaveChanges() == 1)
                return true;
            else
                return false;
        }
        public List<CzytelnikModel> DajListeCzytelnikow()
        {
            return context.Czytelnicy.ToList<CzytelnikModel>();
        }
        public string DajTytulDlaEgzemplarza(int IdEgzemplarza)
        {
            return context.Database.SqlQuery<string>("SELECT Tytul FROM Ksiazka k RIGHT JOIN Egzemplarz e on k.id_ksiazka = e.KsiazkaModel_Id WHERE e.id_egzemplarz = " + IdEgzemplarza).FirstOrDefault();
        }
        public List<CzytelnikModel> DajListeCzytelnikow(string imie ="", string nazwisko = "", string identyfikator = "")
        {
            imie = imie ?? "";
            nazwisko = nazwisko ?? "";
            identyfikator = identyfikator ?? "";
            List<CzytelnikModel> listaCzytelnikow = new List<CzytelnikModel>();
            Boolean poprawny;
            foreach (CzytelnikModel czytelnik in DajListeCzytelnikow())
            {
                poprawny = true;
                if (imie != "")
                {
                    if (czytelnik.Imie != imie)
                        poprawny = false;
                }
                if (identyfikator != "")
                {
                    if (czytelnik.Identyfikator != identyfikator)
                        poprawny = false;
                }
                if (nazwisko != "")
                {
                    if (czytelnik.Nazwisko != nazwisko)
                        poprawny = false;
                }
                if (poprawny == true)
                {
                    listaCzytelnikow.Add(czytelnik);
                }
            }
            return listaCzytelnikow;
        }
        public int DajLiczbeWypozyczonych(int CzytelnikID)
        {
            return context.Database.SqlQuery<int>("SELECT Count(*) FROM(SELECT * FROM Czytelnik WHERE id_czytelnik = " + CzytelnikID + ") as c INNER JOIN Wypozyczenie w on c.id_czytelnik = w.Czytelnik_Id WHERE w.DataZwrotu is null OR w.DataZwrotu < w.DataWypozyczenia").SingleOrDefault();
        }
        public List<KsiazkaModel> DajListeKsiazek(string tytul = "", string autor = "", string wydawnictwo = "")
        {
            tytul = tytul ?? "";
            autor = autor ?? "";
            wydawnictwo = wydawnictwo ?? "";
            Boolean poprawny;
            List<KsiazkaModel> listaKsiazek = new List<KsiazkaModel>();
            foreach (KsiazkaModel ksiazka in (context.Ksiazki.Include("Autor").Include("Wydawnictwo").ToList()))
            {
                poprawny = true;
                if (tytul != "")
                {
                    if (ksiazka.Tytul.ToUpper() != tytul.ToUpper())
                        poprawny = false;
                }
                if (autor != "")
                {
                    if (((ksiazka?.Autor?.Nazwisko ?? null)==null ) || ksiazka.Autor.Nazwisko.ToUpper() != autor.ToUpper())
                        poprawny = false;
                }
                if (wydawnictwo != "")
                {
                    if (((ksiazka?.Wydawnictwo?.NazwaWydawnictwa ?? null) == null) || ksiazka.Wydawnictwo.NazwaWydawnictwa.ToUpper() != wydawnictwo.ToUpper())
                        poprawny = false;
                }
                if (poprawny == true)
                {
                    listaKsiazek.Add(ksiazka);
                }   
            }
            return listaKsiazek;
        }
    }
}
