﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    public class UzytkownikModel : OsobaModel
    {
        public UzytkownikModel()
        {
        }

        public UzytkownikModel(string imie, string nazwisko) : base(imie, nazwisko)
        {
        }

        public UzytkownikModel(string imie, string nazwisko, string login, string haslo) : base(imie, nazwisko)
        {
            Login = login;
            Haslo = haslo;
        }
        public UzytkownikModel(string imie, string nazwisko, DateTime dataUrodzenia, string login, string haslo) : base(imie, nazwisko, dataUrodzenia)
        {
            Login = login;
            Haslo = haslo;
        }

        public string Login { get; set; }
        public string Haslo { get; set; }
    }
}
