﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Administrator")]
    public class AdministratorModel : UzytkownikModel
    {
        public AdministratorModel()
        {
        }

        public AdministratorModel(string imie, string nazwisko) : base(imie, nazwisko)
        {
        }

        [Key]
        [Column("id_administrator")]
        public int ID { get; set; }
        public ICollection<BibliotekarzModel> Bibliotekarze { get; set; }
    }
}
