﻿using Biblioteka_Szkolna.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna
{
    [Table("Ksiazka")]
    public class KsiazkaModel
    {
        [Key]
        [Column("id_ksiazka")]
        public int Id { get; set; }
        public string Tytul { get; set; }
        public string Isbn { get; set; }
        public string Opis { get; set; }
        public AutorModel Autor { get; set; }
        public WydawnictwoModel Wydawnictwo { get; set; }
        public ICollection<EgzemplarzModel> Egzemplarze { get; set; }

        public KsiazkaModel()
        {
            Autor = new AutorModel();
            Wydawnictwo = new WydawnictwoModel();
        }

        public KsiazkaModel(string tytul)
        {
            Tytul = tytul;
        }

        public KsiazkaModel(string tytul, string isbn, string opis, AutorModel autor)
        {
            Tytul = tytul;
            Isbn = isbn;
            Opis = opis;
            Autor = autor;
        }
        public KsiazkaModel(string tytul, string isbn, AutorModel autor)
        {
            Tytul = tytul;
            Isbn = isbn;
            Autor = autor;
        }
        public KsiazkaModel(string tytul, string isbn, string opis, AutorModel autor, WydawnictwoModel wydawnictwo)
        {
            Tytul = tytul;
            Isbn = isbn;
            Opis = opis;
            Autor = autor;
            Wydawnictwo = wydawnictwo;
        }
        public KsiazkaModel(string tytul, string isbn, AutorModel autor, WydawnictwoModel wydawnictwo)
        {
            Tytul = tytul;
            Isbn = isbn;
            Autor = autor;
            Wydawnictwo = wydawnictwo;
        }
        public Boolean PoprawneDaneFiltrowania()
        {
            if ((Tytul != "" && Tytul != null) || (Autor != null && Autor.Nazwisko != "" && Autor.Nazwisko != null) || (Wydawnictwo != null && Wydawnictwo.NazwaWydawnictwa != "" && Wydawnictwo.NazwaWydawnictwa != null))
            {
                return true;
            }
            else
                return false;
        }

        
    }
}
