﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Autor")]
    public class AutorModel : OsobaModel
    {
        [Key]
        [Column("id_autor")]
        public int Id { get; set; }
        public ICollection<KsiazkaModel> Ksiazki { get; set; }
        public string Biografia { get; set; }

        public AutorModel(string imie, string nazwisko, string biografia) : base (imie, nazwisko)
        {
            Biografia = biografia;
        }

        public AutorModel(string imie, string nazwisko): base(imie, nazwisko)
        {
        }

        public AutorModel()
        {
        }
        public override string ToString()
        {
            return Imie+ " " + Nazwisko;
        }
    }
}
