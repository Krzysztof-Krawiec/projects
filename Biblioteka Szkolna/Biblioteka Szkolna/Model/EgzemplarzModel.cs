﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Egzemplarz")]
    public class EgzemplarzModel
    {
        public EgzemplarzModel()
        {
        }

        public EgzemplarzModel(string numerEgzemplarza, int iloscStron)
        {
            NumerEgzemplarza = numerEgzemplarza;
            IloscStron = iloscStron;
        }

        [Key]
        [Column("id_egzemplarz")]
        public int Id { get; set; }
        public string NumerEgzemplarza { get; set; }
        public int IloscStron { get; set; }
    }
}
