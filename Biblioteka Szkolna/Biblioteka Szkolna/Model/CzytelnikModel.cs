﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Czytelnik")]
    public class CzytelnikModel : UzytkownikModel
    {
        [Key]
        [Column("id_czytelnik")]
        public int Id { get; set; }
        public string Identyfikator { get; set; }
        public ICollection<WypozyczenieModel> Wypozyczenia { get; set; }

        public CzytelnikModel()
        {
        }

        public CzytelnikModel(string imie, string nazwisko, string identyfikator, string login, string haslo) : base(imie, nazwisko, login, haslo)
        {
            Identyfikator = identyfikator;
        }
        public CzytelnikModel(string imie, string nazwisko, string identyfikator) : base(imie, nazwisko)
        {
            Identyfikator = identyfikator;
        }
        
        public void KopiujDane(CzytelnikModel kopier)
        {
            Id = kopier.Id;
            Identyfikator = kopier.Identyfikator;
            Login = kopier.Login;
            Haslo = kopier.Haslo;
            Imie = kopier.Imie;
            Nazwisko = kopier.Nazwisko;
            DataUrodzenia = new DateTime(kopier.DataUrodzenia.Year, kopier.DataUrodzenia.Month, kopier.DataUrodzenia.Day, kopier.DataUrodzenia.Hour, kopier.DataUrodzenia.Minute, kopier.DataUrodzenia.Second);
        }

        public Boolean PoprawneDaneFiltrowania()
        {
            if ((Identyfikator != "" && Identyfikator != null) || (Imie != "" && Imie != null) || (Nazwisko != "" && Nazwisko != null))
            {
                return true;
            }
            else
                return false;
        }
        public override string ToString()
        {
            return Imie + " " + Nazwisko;
        }
    }
}
