﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Wydawnictwo")]
    public class WydawnictwoModel
    {
        [Key]
        [Column("id_wydawnictwo")]
        public int Id { get; set; }
        public string NazwaWydawnictwa { get; set; }
        public WydawnictwoModel()
        {
        }

        public WydawnictwoModel(string nazwaWydawnictwa)
        {
            NazwaWydawnictwa = nazwaWydawnictwa;
        }
        public override string ToString()
        {
            return NazwaWydawnictwa;
        }



    }
}
