﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Model
{
    [Table("Bibliotekarz")]
    public class BibliotekarzModel : UzytkownikModel
    {
        public BibliotekarzModel()
        {
        }

        public BibliotekarzModel(string imie, string nazwisko, string login, string haslo) : base(imie, nazwisko, login, haslo)
        {
        }

        [Key]
        [Column("id_bibliotekarz")]
        public int Id { get; set; }
    }
}
