﻿using Biblioteka_Szkolna.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka_Szkolna.Kontekst
{
    class BibliotekaKontekst : DbContext
    {
        public DbSet<AdministratorModel> Students { get; set; }
        public DbSet<AutorModel> Autorzy { get; set; }
        public DbSet<BibliotekarzModel> Bibliotekarze { get; set; }
        public DbSet<CzytelnikModel> Czytelnicy { get; set; }
        public DbSet<EgzemplarzModel> Egzemplarze { get; set; }
        public DbSet<KsiazkaModel> Ksiazki { get; set; }
        public DbSet<WydawnictwoModel> Wydawnictwa { get; set; }
        public DbSet<WypozyczenieModel> Wypozyczenia { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
