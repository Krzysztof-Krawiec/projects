# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 2: k-NN i Naive Bayes
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

from __future__ import division

import numpy as np


def hamming_distance(X, X_train):
    """
    :param X: zbior porownwanych obiektow N1xD
    :param X_train: zbior obiektow do ktorych porownujemy N2xD
    Funkcja wyznacza odleglosci Hamminga obiektow ze zbioru X od
    obiektow X_train. Odleglosci obiektow z jednego i drugiego
    zbioru zwrocone zostana w postaci macierzy
    :return: macierz odleglosci pomiedzy obiektami z X i X_train N1xN2
    """
    N1, _ = np.shape(X)
    N2, _ = np.shape(X_train)
    result = np.zeros((N1, N2))


    X_array=X.toarray()
    X_train_array = X_train.toarray()
    for i in range(N1):
        xor_arr = np.logical_xor(X_array[i], X_train_array)
        row = np.sum(xor_arr, axis=1)
        result[i]=row
    return result
    pass



def sort_train_labels_knn(Dist, y):
    """
    Funkcja sortujaca etykiety klas danych treningowych y
    wzgledem prawdopodobienstw zawartych w macierzy Dist.
    Funkcja zwraca macierz o wymiarach N1xN2. W kazdym
    wierszu maja byc posortowane etykiety klas z y wzgledem
    wartosci podobienstw odpowiadajacego wiersza macierzy
    Dist
    :param Dist: macierz odleglosci pomiedzy obiektami z X
    i X_train N1xN2
    :param y: wektor etykiet o dlugosci N2
    :return: macierz etykiet klas posortowana wzgledem
    wartosci podobienstw odpowiadajacego wiersza macierzy
    Dist. Uzyc algorytmu mergesort.


    macierz = Dist
    N1, N2 = np.shape(macierz)
    etykiety = y
    result = np.zeros((N1, N2))
    pom = []
    for listiterator in range(N2):
        pom.append(0)
    pom2=np.array(pom,dtype = [('distance', int),  ('x', int)])
    for matrixY in range(N1):
        for matrixX in range(N2):
            pom2[matrixX] = (macierz[matrixY][matrixX], matrixX)
        pom2 = np.sort(pom2, kind='mergesort', order='distance')

        for i in range(N2):
            result[matrixY][i] = etykiety[pom2[i][1]]"""

    macierz = Dist
    N1, N2 = np.shape(macierz)
    etykiety = y
    result = np.zeros((N1, N2))
    pom = []
    for listiterator in range(N2):
        pom.append(0)

    for matrixY in range(N1):
        pom2 = np.array(pom, dtype=[('distance', int), ('x', int)])
        for matrixX in range(N2):
            pom2[matrixX] = (macierz[matrixY][matrixX], matrixX)
        pom2 = np.sort(pom2, kind='mergesort', order='distance')

        for i in range(N2):
            result[matrixY][i] = etykiety[pom2[i][1]]

    return result



    pass


def p_y_x_knn(y, k):
    """
    Funkcja wyznacza rozklad prawdopodobienstwa p(y|x) dla
    kazdej z klas dla obiektow ze zbioru testowego wykorzystujac
    klasfikator KNN wyuczony na danych trenningowych
    :param y: macierz posortowanych etykiet dla danych treningowych N1xN2
    :param k: liczba najblizszuch sasiadow dla KNN
    :return: macierz prawdopodobienstw dla obiektow z X
    """
    matrix_size = np.max(y+1)
    a, b = np.shape(y)
    result = np.zeros((a, int(matrix_size)))
    for i in range(a):
        for j in range(k):
            index = int(y[i][j])
            result[i][index ] = result[i][index ] + 1
        for j in range(int(matrix_size)):
            result[i][j] = result[i][j] / k
    return result
    pass


def classification_error(p_y_x, y_true):
    """
    Wyznacz blad klasyfikacji.
    :param p_y_x: macierz przewidywanych prawdopodobienstw
    :param y_true: zbior rzeczywistych etykiet klas 1xN.
    Kazdy wiersz macierzy reprezentuje rozklad p(y|x)
    :return: blad klasyfikacji
    """

    n = y_true.size
    pom = 0
    for i in range(n):
        _, pomDlugosc = np.shape(p_y_x)
        najwieksze = 0
        index = 0
        for j in range(pomDlugosc):
            if (p_y_x[i][j] >= najwieksze):
                index = j
                najwieksze = p_y_x[i][j]
        if (y_true[i] ) != index:
            pom = pom + 1

    return pom / n
    pass


def model_selection_knn(Xval, Xtrain, yval, ytrain, k_values):
    """
    :param Xval: zbior danych walidacyjnych N1xD
    :param Xtrain: zbior danych treningowych N2xD
    :param yval: etykiety klas dla danych walidacyjnych 1xN1
    :param ytrain: etykiety klas dla danych treningowych 1xN2
    :param k_values: wartosci parametru k, ktore maja zostac sprawdzone
    :return: funkcja wykonuje selekcje modelu knn i zwraca krotke (best_error,best_k,errors), gdzie best_error to najnizszy
    osiagniety blad, best_k - k dla ktorego blad byl najnizszy, errors - lista wartosci bledow dla kolejnych k z k_values
    """

    blad = 10000
    result_k = 0
    errors = []
    sorted_train_labels_knn= sort_train_labels_knn(hamming_distance(Xval, Xtrain), ytrain)
    for i in k_values:
        pom = classification_error(p_y_x_knn(sorted_train_labels_knn, i), yval)
        if pom < blad:
            blad = pom
            result_k = i
        errors.append(pom)

    return (blad, result_k, errors)

    pass


def estimate_a_priori_nb(ytrain):
    """
    :param ytrain: etykiety dla dla danych treningowych 1xN
    :return: funkcja wyznacza rozklad a priori p(y) i zwraca p_y - wektor prawdopodobienstw a priori 1xM
    """
    y = ytrain.size
    max_y = np.max(ytrain+1)
    result = []
    for i in range(max_y):
        result.append(0)
    for i in range(y):
        pom = ytrain[i]
        result[pom] = result[pom] + 1
    for i in range(max_y):
        result[i] = result[i] / y
    return result
    pass


def estimate_p_x_y_nb(Xtrain, ytrain, a, b):
    """
    :param Xtrain: dane treningowe NxD
    :param ytrain: etykiety klas dla danych treningowych 1xN
    :param a: parametr a rozkladu Beta
    :param b: parametr b rozkladu Beta
    :return: funkcja wyznacza rozklad prawdopodobienstwa p(x|y) zakladajac, ze x przyjmuje wartosci binarne i ze elementy
    x sa niezalezne od siebie. Funkcja zwraca macierz p_x_y o wymiarach MxD.
    """
    _, d = np.shape(Xtrain)
    m = np.max(ytrain+1)
    result = np.zeros((m, d))
    n = np.size(ytrain)
    Xtrain_array = Xtrain.toarray()
    for i in range(d):
        pom = []
        for pomi in range(m):
            pom.append((0, 0))
        for j in range(n):
            (f, s) = pom[ytrain[j] ]
            s = s + 1
            if (Xtrain_array[j, i]):
                f = f + 1
            pom[ytrain[j] ] = (f, s)
        for pomi in range(m):
            (f, s) = pom[pomi]
            result[pomi][i] = (f + a - 1) / (s + a + b - 2)

    return result
    pass


def p_y_x_nb(p_y, p_x_1_y, X):
    """
    :param p_y: wektor prawdopodobienstw a priori o wymiarach 1xM
    :param p_x_1_y: rozklad prawdopodobienstw p(x=1|y) - macierz MxD
    :param X: dane dla ktorych beda wyznaczone prawdopodobienstwa, macierz NxD
    :return: funkcja wyznacza rozklad prawdopodobienstwa p(y|x) dla kazdej z klas z wykorzystaniem klasyfikatora Naiwnego
    Bayesa. Funkcja zwraca macierz p_y_x o wymiarach NxM.
    """
    K = np.size(p_y)
    N, D = np.shape(X)
    result = np.ones((N, K))
    X_array =X.toarray()

    for n in range(N):
        for d in range(D):
            for k in range(K):
                if X_array[n, d]:
                    result[n][k] = result[n][k] * p_x_1_y[k][d]
                else:
                    result[n][k] = result[n][k] * (1 - p_x_1_y[k][d])
        suma = 0
        for k in range(K):
            result[n, k] = result[n, k] * p_y[k]
            suma += result[n, k]

        for k in range(K):
            result[n, k] = result[n, k] / suma

    return result

    pass


def model_selection_nb(Xtrain, Xval, ytrain, yval, a_values, b_values):
    """
    :param Xtrain: zbior danych treningowych N2xD
    :param Xval: zbior danych walidacyjnych N1xD
    :param ytrain: etykiety klas dla danych treningowych 1xN2
    :param yval: etykiety klas dla danych walidacyjnych 1xN1
    :param a_values: lista parametrow a do sprawdzenia
    :param b_values: lista parametrow b do sprawdzenia
    :return: funkcja wykonuje selekcje modelu Naive Bayes - wybiera najlepsze wartosci parametrow a i b. Funkcja zwraca
    krotke (error_best, best_a, best_b, errors) gdzie best_error to najnizszy
    osiagniety blad, best_a - a dla ktorego blad byl najnizszy, best_b - b dla ktorego blad byl najnizszy,
    errors - macierz wartosci bledow dla wszystkich par (a,b)
    """

    blad = 10000
    result_k = 0
    errors = np.zeros((np.size(a_values), np.size(b_values)))
    best_a = 999
    best_b = 999
    i=0
    estimated_a_priori_nb=estimate_a_priori_nb(ytrain)
    for a in a_values:
        j=0
        for b in b_values:
            pom = classification_error(
                p_y_x_nb(estimated_a_priori_nb, estimate_p_x_y_nb(Xtrain, ytrain, a, b), Xval), yval)
            if pom < blad:
                blad = pom
                best_a = a
                best_b = b
            errors[i][j]=(pom)
            j=j+1
        i=i+1

    return (blad, best_a, best_b, errors)

    pass
