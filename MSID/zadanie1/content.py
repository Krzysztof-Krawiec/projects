# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 1: Regresja liniowa
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import numpy as np
from utils import polynomial


def mean_squared_error(x, y, w):
    '''
    :param x: ciag wejsciowy Nx1
    :param y: ciag wyjsciowy Nx1
    :param w: parametry modelu (M+1)x1
    :return: blad sredniokwadratowy pomiedzy wyjsciami y
    oraz wyjsciami uzyskanymi z wielowamiu o parametrach w dla wejsc x
    '''

    n = x.shape[0];
    m = w.shape[0];

    abc = polynomial(x,w)
    suma = 0
    for i in range(n):
        suma += (y[i] - abc[i]) ** 2
    return (suma[0]) / n



    pass

def design_matrix(x_train, M):
    '''
    :param x_train: ciag treningowy Nx1
    :param M: stopien wielomianu 0,1,2,...
    :return: funkcja wylicza Design Matrix Nx(M+1) dla wielomianu rzedu M
    '''
    n = x_train.shape[0]
    result_matrix = np.zeros(shape=(n, M + 1))
    for i in range(M + 1):
        result_matrix[:, i] = x_train[:, 0] ** i
    return result_matrix
    pass


def least_squares(x_train, y_train, M):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu, a err blad sredniokwadratowy
    dopasowania
    '''

    result = design_matrix(x_train, M)
    w = (np.linalg.inv((result.transpose()@result)))@result.transpose()@y_train
    blad = mean_squared_error(x_train, y_train, w)
    return (w, blad)
    pass


def regularized_least_squares(x_train, y_train, M, regularization_lambda):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :param regularization_lambda: parametr regularyzacji
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu zgodnie z kryterium z regularyzacja l2,
    a err blad sredniokwadratowy dopasowania
    '''

    res = design_matrix(x_train, M)
    _, i = np.shape(res)
    identity = np.identity(i)
    w = np.linalg.inv(res.transpose()@res+regularization_lambda*identity)@res.transpose()@y_train
    return (w, mean_squared_error(x_train, y_train,w))

    pass


def model_selection(x_train, y_train, x_val, y_val, M_values):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M_values: tablica stopni wielomianu, ktore maja byc sprawdzone
    :return: funkcja zwraca krotke (w,train_err,val_err), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym, train_err i val_err to bledy na sredniokwadratowe na ciagach treningowym
    i walidacyjnym
    '''
    M = len(M_values)
    best_train_err=999999
    best_val_err=9999999
    best_w=9999999

    for i in M_values:
        (w, train_err) = least_squares(x_train, y_train, i)
        val_err = mean_squared_error(x_val, y_val, w)
        if val_err < best_val_err :
            best_train_err = train_err
            best_val_err = val_err
            best_w = w

    return (best_w,best_train_err,best_val_err)

    pass


def regularized_model_selection(x_train, y_train, x_val, y_val, M, lambda_values):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M: stopien wielomianu
    :param lambda_values: lista ze wartosciami roznych parametrow regularyzacji
    :return: funkcja zwraca krotke (w,train_err,val_err,regularization_lambda), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym. Wielomian dopasowany jest wg kryterium z regularyzacja. train_err i val_err to
    bledy na sredniokwadratowe na ciagach treningowym i walidacyjnym. regularization_lambda to najlepsza wartosc parametru regularyzacji
    '''

    best_train_err = 999999
    best_val_err = 9999999
    best_w = 9999999
    best_lambda = 999999
    lambda_length =len(lambda_values)

    for i in range (lambda_length):
        (w, train_err) = regularized_least_squares(x_train, y_train, M, lambda_values[i])
        val_err = mean_squared_error(x_val, y_val, w)
        if val_err < best_val_err:
            best_train_err = train_err
            best_val_err = val_err
            best_w = w
            best_lambda = lambda_values[i]

    return (best_w, best_train_err, best_val_err, best_lambda)
    pass
