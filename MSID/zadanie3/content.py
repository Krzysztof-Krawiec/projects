# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 3: Regresja logistyczna
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import numpy as np


def sigmoid(x):
    """
    :param x: wektor wejsciowych wartosci Nx1
    :return: wektor wyjściowych wartości funkcji sigmoidalnej dla wejścia x, Nx1
    """
    x_size = np.size(x)
    result = np.zeros((x_size,1))
    for i in range(x_size):
        result[i]=1/(1+np.exp(-x[i]))
    return result
    pass

def logistic_cost_function(w, x_train, y_train):
    """
    :param w: parametry modelu Mx1
    :param x_train: ciag treningowy - wejscia NxM
    :param y_train: ciag treningowy - wyjscia Nx1
    :return: funkcja zwraca krotke (val, grad), gdzie val oznacza wartosc funkcji logistycznej, a grad jej gradient po w
    """
    N,_ = np.shape(x_train)
    val = 0
    L=0
    for n in range(N):
        pom = (sigmoid(w.transpose()@x_train[n]))[0]
        val = val+((y_train[n][0] * np.log(pom)) + (1 - y_train[n][0])*np.log(1 - pom))[0]

    L = -val/N

    pom = sigmoid( x_train @ w)
    return L, - (x_train.transpose() @ (y_train - pom)) / y_train.shape[0]






    pass

def gradient_descent(obj_fun, w0, epochs, eta):
    """
    :param obj_fun: funkcja celu, ktora ma byc optymalizowana. Wywolanie val,grad = obj_fun(w).
    :param w0: punkt startowy Mx1
    :param epochs: liczba epok / iteracji algorytmu
    :param eta: krok uczenia
    :return: funkcja wykonuje optymalizacje metoda gradientu prostego dla funkcji obj_fun. Zwraca krotke (w,func_values),
    gdzie w oznacza znaleziony optymalny punkt w, a func_valus jest wektorem wartosci funkcji [epochs x 1] we wszystkich krokach algorytmu
    """
    result=np.zeros((epochs,1))
    w=w0
    _, grad = obj_fun(w)
    for k in range (epochs):
        w = w - eta * grad
        val, grad = obj_fun(w)
        result[k][0]=val

    return (w,result)
    pass

def stochastic_gradient_descent(obj_fun, x_train, y_train, w0, epochs, eta, mini_batch):
    """
    :param obj_fun: funkcja celu, ktora ma byc optymalizowana. Wywolanie val,grad = obj_fun(w,x,y), gdzie x,y oznaczaja podane
    podzbiory zbioru treningowego (mini-batche)
    :param x_train: dane treningowe wejsciowe NxM
    :param y_train: dane treningowe wyjsciowe Nx1
    :param w0: punkt startowy Mx1
    :param epochs: liczba epok
    :param eta: krok uczenia
    :param mini_batch: wielkosc mini-batcha
    :return: funkcja wykonuje optymalizacje metoda stochastycznego gradientu prostego dla funkcji obj_fun. Zwraca krotke (w,func_values),
    gdzie w oznacza znaleziony optymalny punkt w, a func_values jest wektorem wartosci funkcji [epochs x 1] we wszystkich krokach algorytmu. Wartosci
    funkcji do func_values sa wyliczane dla calego zbioru treningowego!
    """
    w=w0
    result = np.zeros((epochs,1))
    M = int(y_train.shape[0] / mini_batch)
    x_batch = np.vsplit(x_train, M)
    y_batch = np.vsplit(y_train, M)
    for k in range (epochs):
        for m in range (M):
            val, grad = obj_fun(w, x_batch[m], y_batch[m])
            w= -eta*grad+w

        val,_=obj_fun(w, x_train,y_train)
        result[k][0]=val
    return (w,result)
    pass

def regularized_logistic_cost_function(w, x_train, y_train, regularization_lambda):
    """
    :param w: parametry modelu Mx1
    :param x_train: ciag treningowy - wejscia NxM
    :param y_train: ciag treningowy - wyjscia Nx1
    :param regularization_lambda: parametr regularyzacji
    :return: funkcja zwraca krotke (val, grad), gdzie val oznacza wartosc funkcji logistycznej z regularyzacja l2,
    a grad jej gradient po w
    """

    val, grad = logistic_cost_function(w, x_train, y_train)
    w_0 = np.array(w, copy=True)
    w_0[0] = 0
    L=val+regularization_lambda/ 2*np.linalg.norm(w_0)**2

    adding = regularization_lambda*w
    adding[0]=0

    return L,grad+adding


    pass

def prediction(x, w, theta):
    """
    :param x: macierz obserwacji NxM
    :param w: wektor parametrow modelu Mx1
    :param theta: prog klasyfikacji z przedzialu [0,1]
    :return: funkcja wylicza wektor y o wymiarach Nx1. Wektor zawiera wartosci etykiet ze zbioru {0,1} dla obserwacji z x
     bazujac na modelu z parametrami w oraz progu klasyfikacji theta
    """
    pass

    N,_=np.shape(x)
    y = np.zeros((N,1))
    for n in range(N):
        if(sigmoid( x[n] @ w)>theta):
            y[n][0]=1
        else:
            y[n][0] = 0
    return y

def f_measure(y_true, y_pred):
    """
    :param y_true: wektor rzeczywistych etykiet Nx1
    :param y_pred: wektor etykiet przewidzianych przed model Nx1
    :return: funkcja wylicza wartosc miary F
    """

    TP=0

    FF=0
    N,_=np.shape(y_true)
    for n in range(N):
        if(y_pred[n]==y_true[n] and y_pred[n]==1):
            TP=TP+1
        else:
            if(y_pred[n]==1 and y_true[n]==0):
                FF=FF+1
            else:
                if (y_pred[n] == 0 and y_true[n] == 1):
                    FF = FF + 1
    return 2*TP/(2*TP+FF)
    pass

def model_selection(x_train, y_train, x_val, y_val, w0, epochs, eta, mini_batch, lambdas, thetas):
    """
    :param x_train: ciag treningowy wejsciowy NxM
    :param y_train: ciag treningowy wyjsciowy Nx1
    :param x_val: ciag walidacyjny wejsciowy Nval x M
    :param y_val: ciag walidacyjny wyjsciowy Nval x 1
    :param w0: wektor poczatkowych wartosci parametrow
    :param epochs: liczba epok dla SGD
    :param eta: krok uczenia
    :param mini_batch: wielkosc mini batcha
    :param lambdas: lista wartosci parametru regularyzacji lambda, ktore maja byc sprawdzone
    :param thetas: lista wartosci progow klasyfikacji theta, ktore maja byc sprawdzone
    :return: funckja wykonuje selekcje modelu. Zwraca krotke (regularization_lambda, theta, w, F), gdzie regularization_lambda
    to najlpszy parametr regularyzacji, theta to najlepszy prog klasyfikacji, a w to najlepszy wektor parametrow modelu.
    Dodatkowo funkcja zwraca macierz F, ktora zawiera wartosci miary F dla wszystkich par (lambda, theta). Do uczenia nalezy
    korzystac z algorytmu SGD oraz kryterium uczenia z regularyzacja l2.
    """
    F=np.zeros((np.size(lambdas),np.size(thetas)))
    thetas_length = np.size(thetas)
    result_regularization_lambda = 0
    result_theta = 0
    result_w = 0
    result_f= -1
    for i,lam in enumerate (lambdas):

        obj_fun = lambda w, x, y: regularized_logistic_cost_function(w, x, y, lam)
        w, values=stochastic_gradient_descent(obj_fun, x_train, y_train, w0, epochs, eta, mini_batch)


        for j in range(thetas_length):
            f = f_measure(y_val, prediction(x_val,w,thetas[j]))
            F[i,j]=f
            if f > result_f:
                result_regularization_lambda = lam
                result_theta = thetas[j]
                result_w = w
                result_f = f

    return (result_regularization_lambda,result_theta,result_w,F)
    pass
